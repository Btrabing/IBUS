  EMXI	    EP	    12	    24	    36	    48	    60	    72	    84	    96	   108	   120	   132	   144	   156	   168
-127.5	-117.5	   8.6	  14.2	  19.1	  27.4	  30.5	  29.5	  32.2	  39.0	  42.1	  45.1	  37.0	  46.0	  50.1	  36.5
-117.5	-107.5	   8.6	  14.2	  19.1	  27.4	  30.5	  29.5	  32.2	  39.0	  42.1	  45.1	  37.0	  46.0	  50.1	  36.5
-107.5	 -97.5	   8.6	  14.2	  19.1	  27.4	  30.5	  29.5	  32.2	  39.0	  42.1	  45.1	  37.0	  46.0	  50.1	  36.5
 -97.5	 -87.5	   8.6	  14.2	  19.1	  27.4	  30.5	  29.5	  32.2	  39.0	  42.1	  45.1	  37.0	  46.0	  50.1	  36.5
 -87.5	 -77.5	   8.6	  14.2	  19.1	  27.4	  30.5	  29.5	  32.2	  39.0	  42.1	  45.1	  37.0	  46.0	  50.1	  36.5
 -77.5	 -67.5	   8.6	  14.2	  19.1	  27.4	  30.5	  29.5	  32.2	  39.0	  42.1	  45.1	  37.0	  46.0	  50.1	  36.5
 -67.5	 -57.5	   8.6	  14.2	  19.1	  27.4	  30.5	  29.5	  32.2	  39.0	  42.1	  45.1	  37.0	  46.0	  50.1	  36.5
 -57.5	 -47.5	   8.6	  14.2	  19.1	  27.4	  30.5	  29.5	  32.2	  39.0	  42.1	  45.1	  37.0	  46.0	  50.1	  36.5
 -47.5	 -37.5	   8.6	  14.2	  19.1	  27.4	  30.4	  29.6	  31.7	  38.0	  40.7	  43.1	  35.8	  45.2	  49.1	  36.8
 -37.5	 -27.5	   8.6	  13.8	  18.9	  26.8	  29.3	  29.4	  29.3	  33.6	  34.7	  35.3	  31.0	  40.8	  44.3	  36.8
 -27.5	 -17.5	   8.1	  12.1	  17.3	  23.8	  25.4	  26.0	  24.3	  25.7	  24.9	  25.0	  24.8	  31.6	  35.1	  32.7
 -17.5	  -7.5	   6.1	   8.7	  13.0	  16.7	  18.0	  18.5	  18.0	  17.9	  17.6	  18.5	  19.9	  21.1	  23.8	  22.3
  -7.5	  -2.5	   2.5	   3.8	   6.0	   7.6	   9.1	  10.0	  11.4	  11.2	  13.7	  14.1	  15.1	  14.3	  15.3	  13.7
  -2.5	   2.5	  -0.4	  -1.4	  -1.5	  -0.8	   0.8	   2.6	   5.0	   6.0	  10.1	  10.6	  11.2	  11.8	  12.5	  14.2
   2.5	   7.5	  -1.1	  -4.3	  -6.7	  -7.0	  -6.1	  -4.1	  -1.0	   2.7	   6.0	   7.8	   8.7	  10.7	  12.2	  17.3
   7.5	  17.5	  -0.5	  -3.4	  -7.7	  -9.3	 -10.9	  -8.8	  -6.6	  -2.0	   1.2	   3.5	   5.6	   8.8	  10.0	  14.6
  17.5	  27.5	  -0.0	  -1.1	  -5.7	  -8.1	 -12.8	 -11.4	 -10.7	  -7.4	  -4.2	  -1.3	   1.2	   3.4	   4.4	   7.3
  27.5	  37.5	   0.1	   0.2	  -4.2	  -6.6	 -12.5	 -13.4	 -12.4	  -9.2	  -8.4	  -5.5	  -4.3	  -4.1	  -3.1	  -1.2
  37.5	  47.5	   0.1	   0.5	  -3.8	  -6.1	 -12.0	 -14.5	 -12.3	  -7.9	 -10.3	  -9.5	  -9.7	  -9.6	  -9.0	  -7.5
  47.5	  57.5	   0.2	   0.6	  -3.7	  -6.0	 -11.8	 -14.6	 -11.9	  -6.8	 -10.8	 -11.8	 -13.0	 -11.9	 -11.8	 -10.1
  57.5	  67.5	   0.2	   0.6	  -3.7	  -6.0	 -11.8	 -14.7	 -11.8	  -6.5	 -11.0	 -12.4	 -14.2	 -12.5	 -12.5	 -10.8
  67.5	  77.5	   0.2	   0.6	  -3.7	  -6.0	 -11.8	 -14.7	 -11.7	  -6.5	 -11.0	 -12.5	 -14.5	 -12.6	 -12.6	 -10.9
  77.5	  87.5	   0.2	   0.6	  -3.7	  -6.0	 -11.8	 -14.7	 -11.7	  -6.5	 -11.0	 -12.6	 -14.6	 -12.7	 -12.7	 -11.0
  87.5	  97.5	   0.2	   0.6	  -3.7	  -6.0	 -11.8	 -14.7	 -11.7	  -6.5	 -11.0	 -12.6	 -14.6	 -12.7	 -12.7	 -11.0
  97.5	 107.5	   0.2	   0.6	  -3.7	  -6.0	 -11.8	 -14.7	 -11.7	  -6.5	 -11.0	 -12.6	 -14.6	 -12.7	 -12.7	 -11.0
 107.5	 117.5	   0.2	   0.6	  -3.7	  -6.0	 -11.8	 -14.7	 -11.7	  -6.5	 -11.0	 -12.6	 -14.6	 -12.7	 -12.7	 -11.0
 117.5	 127.5	   0.2	   0.6	  -3.7	  -6.0	 -11.8	 -14.7	 -11.7	  -6.5	 -11.0	 -12.6	 -14.6	 -12.7	 -12.7	 -11.0
