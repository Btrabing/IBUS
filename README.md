# The Intensity Bias and Uncertainty Scheme (IBUS)

**Written by Ben Trabing, CIRA/NHC, Ben.Trabing@noaa.gov**

The IBUS is described in detail at https://doi.org/10.1175/WAF-D-22-0074.1

If you use this code, please acknowledge and reference this project with:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Trabing, B. C., K. D. Musgrave, M. DeMaria, and E. Blake, 2022: A simple bias and uncertainty scheme for tropical cyclone intensity change forecasts. Wea. Forecasting, 37 (11), 1957 – 1972, https://doi.org/10.1175/WAF-D-22-0074.1.


## Download

To download the source code to run the IBUS:
> git clone git@gitlab.com:Btrabing/IBUS.git
 

## Directories:
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Config   : Includes namelist, model colors, and consensus models \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Data     : Includes a-decks, b-decks, QKVER files, and distance to land table \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Eval     : Output for evaluation \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Input    : Bias and Uncertainty text files for each model and basin \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Output   : Includes Figure and Text file output \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Scripts  : All scripts to preprocess files, create IBUS, and run IBUS on select models 


## How to Run:

1. Run get_qkver.py

2. Run get_IBUS_input.py

3. (Optional) Run Display_IBUS_input.py

4. Run RUN_IBUS.py\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Runs code from IBUS_main.py to create forecast and Plot_IBUS.py to create plots
5. (Optional) Run Eval_IBUS.py

Make sure that the paths are set in each of the scripts above. 


