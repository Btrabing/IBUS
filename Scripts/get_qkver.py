#!/usr/bin/env python
####################################################
#
# Script to process A-deck files to create QKVER files.
# The QKVER files are forecasts of intensity change
#
# Written by Ben Trabing
# Ben.Trabing@noaa.gov
#
####################################################
import subprocess
import numpy as np
import glob
import datetime as dt
import sys
import warnings
warnings.filterwarnings("ignore")

from IBUS_util import check_if_land
from IBUS_util import read_gdland
from IBUS_util import Config_
####################################################

Path_to_adecks='../Data/AID/'
Path_to_bdecks='../Data/BTK/'

YEAR1 = 2017
YEAR2 = 2021   #To use just 1 year, set to same as YEAR1

BASIN = 'al'   #'al' or 'ep' Not used if MAKE_ALL=True
MODEL = 'HWFI' #Not used if MAKE_ALL=True

MAKE_ALL=False #If MAKE_ALL=True, then all models and basins will be run from the config file

OUTDIR = '../Data/QKVER/'

####################################################

def datafgrep(bashcommand):
    try:
        output = subprocess.check_output(bashcommand, shell=True)
    except subprocess.CalledProcessError as exc:
        return exc.output

    output = str(output).split('\\n')
    
    data = []
    for i,val in enumerate(output):
        #A b' shows up when converting from bytes to string
        if i==0:
            data.append(val[2:].split(','))
            
        elif len(val)>=10:
            data.append(val.split(','))
            
    return np.array(data)


def bestgrep(bashcommand):

    output = subprocess.check_output(bashcommand, shell=True)
 
    output = str(output).split('\\n')

    data = []
    for i,val in enumerate(output):
        #A b' shows up when converting from bytes to string
        if (i==0):
            length = len(val.split(','))
            ntoad = 44-int(length)
            new_val = val+ntoad*', '
            data.append(new_val[2:].split(','))

        elif len(val.split(','))>4:
            length = len(val.split(','))
            ntoad = 44-int(length)
            new_val = val+ntoad*', '
            data.append(new_val.split(','))


    return np.array(data)



def get_model_deck(MODEL,STORMPATH):
    bash_arg = 'grep '+MODEL+ ' '+ STORMPATH
    return datafgrep(bash_arg)


def get_model_bdeck(STORMPATH):
    bash_arg = 'grep BEST '+ STORMPATH
    return bestgrep(bash_arg)



def get_Storm_aids(sname,MODEL,FILEPATH,FDATE=False):

    YYYY=sname[-4:]
    BASIN=sname[:2]
    
    
    #Get a list of all the files
    T_filelist= sorted(glob.glob(FILEPATH+'a'+sname+'.dat')) 
    
    filelist = [] #Make a new list which will be iterated over
    for filename in T_filelist:
        stormid = (filename.split('/')[-1])[3:5] #Get the Storm ID#
        if int(stormid)<=45: #Don't include any invest storms
            filelist.append(filename)
        
    #Create the array by using the first case and then adding to it
    ALLDat = get_model_deck(MODEL,filelist[0])
    print('Getting %s Data for %s'%(MODEL,filelist[0].split('/')[-1]))
    
    for filename in filelist[1:]: #Loop through subsequent arrays
        print('Getting %s Data for %s'%(MODEL,filename.split('/')[-1]))
        ALLDat = np.vstack((ALLDat,get_model_deck(MODEL,filename)))
        
    if FDATE:
        return ALLDat[[np.char.strip(ALLDat[:,2])==FDATE]]
    
    else:
        return ALLDat

def remove_multi_afor(adeck):
    # This function removes multiple forecast rows from multiple wind radii
    if len(adeck)==0:
        return []

    fh_list = []
    fr_list = []

    ALLDATA=[]
    for i,line in enumerate(adeck):
        fradii = int(line[11].strip())
        fhour = int(line[5].strip())

        if fradii>35:
            continue
        else:
            ALLDATA.append(line)

    return ALLDATA



        
def get_Storm_best(sname,FILEPATH,FDATE=False):
    
    YYYY=sname[-4:]
    BASIN=sname[:2]
    
    
    #Get a list of all the files
    T_filelist= sorted(glob.glob(FILEPATH+'b'+sname+'.dat')) 
    
    filelist = [] #Make a new list which will be iterated over
    for filename in T_filelist:
        stormid = (filename.split('/')[-1])[3:5] #Get the Storm ID#
        if int(stormid)<=45: #Don't include any invest storms
            filelist.append(filename)
        
    #Create the array by using the first case and then adding to it
    ALLDat = get_model_bdeck('BEST',filelist[0])
    print('Getting %s Data for %s'%('BEST',filelist[0].split('/')[-1]))
    
    for filename in filelist[1:]: #Loop through subsequent arrays
        print('Getting %s Data for %s'%('BEST',filename.split('/')[-1]))
        ALLDat = np.vstack((ALLDat,get_model_bdeck('BEST',filename)))
        
    if FDATE:
        return ALLDat[[np.char.strip(ALLDat[:,2])==FDATE]]
    
    else:
        return ALLDat


def get_ic(adeck,bdeck,SNAME,gdland):
    #This code calculates all the intensity change for all forecast lengths
    #

    ALLDAT = []

    Verifying_STYPES = np.array(['TS','HU','SS','TD','SD']) #Verifying storm types


    copied_dt=[]
    for row_verif in (bdeck):
        SDATE = dt.datetime.strptime(row_verif[2].strip(),'%Y%m%d%H')
        LON  = np.float(row_verif[7].strip()[:-1])/10.
        LAT = np.float(row_verif[6].strip()[:-1])/10.
        INT = np.float(row_verif[8].strip())
        STYPE = (row_verif[10].strip())  
      
        #If the initial storm type is not verifiable
        if (~np.any(Verifying_STYPES==STYPE)):
            continue

        if (np.any(SDATE==np.array(copied_dt))):
            continue
        else:
            copied_dt.append(SDATE)


        f00_int = [] # Initial intensity in adeck (may be different than bdeck)
        f00_dt = [] #This is the initialization time in adeck
        for row_forc in adeck[:]:
            fstormname = row_forc[1].strip()
            fDatetime = dt.datetime.strptime(row_forc[2].strip(),'%Y%m%d%H')
            fModel = row_forc[4].strip()
            fhour = int(row_forc[5].strip())
            fverif = fDatetime + dt.timedelta(hours=int(fhour))
            fLON  = -np.float(row_forc[7].strip()[:-1])/10.
            fLAT = np.float(row_forc[6].strip()[:-1])/10.
            fVINT = np.float(row_forc[8].strip())
            fSTYPE = (row_forc[10].strip())
            fDTL = check_if_land([fhour],[fLON+360.],[fLAT],gdland)

            if fhour==0: #Don't calculate intensity change for f00, instead add to list
                f00_int.append(fVINT)
                f00_dt.append(fDatetime)

            elif (fverif==SDATE)&(fhour>=6): #If we have a verification time and positive forecast hour
                for vm0,fdt0 in zip(f00_int,f00_dt): #loop though saved initialization time 
                    if fdt0==fDatetime:
                        vinit=vm0 #Save initial intensity from the adeck to calculate intensity change

                #Now loop though bdeck to get initialiaztion intensity
                for line in bdeck:
                    if np.char.strip(line[2])== fDatetime.strftime('%Y%m%d%H'):

                        VSTYPE = (line[10].strip())
                        VLON   = -np.float(line[7].strip()[:-1])/10.
                        VLAT   = np.float(line[6].strip()[:-1])/10.
                        VINT   = np.float(line[8].strip()) 
#                        VDTL = check_if_land([np.nan],[VLON+360.],[VLAT],gdland)
 
                        if (~np.any(Verifying_STYPES==VSTYPE)): #Make sure the storm is a verifying storm type
                            continue

                        #Now lets save the data 
                        ALLDAT.append((SNAME,fDatetime.strftime('%Y%m%d%H'),fhour,VLON,VLAT,VINT,INT-VINT,fVINT-vinit,fDTL))
                        break                  
                else:
                    continue
                
    return ALLDAT

def sort_ic(DATA,MODEL):
    # Sorts intensity change by forecast length and then prints data
    # Note that the forecast length is Data index 2: Data[:,2]
    # Number of output variables is preset to 8
    # Forecast ours preset to 12 hourly


    forec_h = [12,24,36,48,60,72,84,96,108,120,132,144,156,168]
    forecasts = [list() for num in range(0,len(forec_h))]

    #First go through the data and sort by forecast hour into defined lists
    for line in DATA:
        for fh,flist in zip(forec_h,forecasts):
            if int(line[2])==fh: #If forecast hours match, add the data to the list
                flist.append(line)

    ###########################
    #Now that all the data is sorted into lists, loop through them and send to stdout
    for fh,flist in zip(forec_h,forecasts):
        print(' {:>4} {:>10} {:>12} {:>6} {:>6} {:>6} {:>6} {:>6} {:>6} '.format(' ','DELV',('t= %s hr'%fh),'V(0)','LAT(0)','LON(0)','BTRK',MODEL,'F_DTL'))
        for ind_,line in enumerate(flist):
            print(' {:>4} {:>10} {:>12} {:>6} {:>6} {:>6} {:>6} {:>6} {:>6} '.format(ind_,line[0],line[1],line[5],line[4],line[3],line[6],line[7],line[8]))
        print('\n')



def main(MODEL,BASIN):
    '''
    This is the main function in this code that calls the above functions and provides output


    '''
    DATA = np.empty([0,9]) # 9 is the number of variables in the output array

    #read in the distance to land variable
    gdland = read_gdland('../Data/gdland_table.dat')


    for year in range(YEAR1,YEAR2+1): #Loop through the number of years you want
        #Now we will loop through all the storms 
        for bdeckstorm in sorted(glob.glob(Path_to_bdecks+'b'+BASIN.lower()+'[0-3][0-9]*'+str(year)+'.dat')):
            SNAME = bdeckstorm.split('/')[-1][1:] #Get the storm name i.e. al032020.dat

            adeckstorm = Path_to_adecks+'a'+SNAME #Get full path to adeck for specific storm
            BDECKDAT = (get_model_bdeck(bdeckstorm)) #Get the bdeck data
            ADECKDAT = remove_multi_afor(get_model_deck(MODEL,adeckstorm)) #get the adeck data

            #Use the adeck and bdecks to get intensity change array
            SDAT = get_ic(ADECKDAT,BDECKDAT,SNAME[:-4],gdland) #Gets all forecast hours in no specified order

            if len(SDAT)==0: #If there was no intensity change forecasts (Short lived or EX)
                continue
            
            #Add the stormdata to the initialized data array
            DATA = np.append(DATA, np.array(SDAT), axis=0) 

    #This function sorts the forecast hours and prints the output variables
    sort_ic(DATA,MODEL)

    sys.stdout.close() #Close the output file


#####################################################################################
#####################################################################################
#####################################################################################

if __name__=='__main__':

    if MAKE_ALL!=True:

        #Determine the outputfile name
        if YEAR1==YEAR2:
            sys.stdout = open((OUTDIR+'qkver_%s_B%s_%s.dat'%(YEAR1,BASIN,MODEL)), 'w')
        else:
            sys.stdout = open((OUTDIR+'qkver_%s-%s_B%s_%s.dat'%(YEAR1,YEAR2,BASIN,MODEL)), 'w')

        main(MODEL,BASIN)

    elif MAKE_ALL==True:
        BASINS=['AL','EP']
        MODELNAMES=Config_.Model_List

        for BASIN in BASINS:
            for MODELNAME in MODELNAMES:

                #Determine the outputfile name
                if YEAR1==YEAR2:
                    sys.stdout = open((OUTDIR+'qkver_%s_B%s_%s.dat'%(YEAR1,BASIN,MODELNAME)), 'w')
                else:
                    sys.stdout = open((OUTDIR+'qkver_%s-%s_B%s_%s.dat'%(YEAR1,YEAR2,BASIN,MODELNAME)), 'w')

                main(MODELNAME,BASIN)




