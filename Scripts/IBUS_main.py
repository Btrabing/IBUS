#!/usr/bin/env python
###############################################

import datetime as dt
import glob
import numpy as np

import warnings
warnings.filterwarnings("ignore")



from IBUS_util import *
from read_IBUS_input import *

###############################################
'''
Within this code are 2 functions:
    BC_Values - Applies the IBUS to forecasts
    run_IBUS - Runs functions above and creates the output file with the relevent data
    
    
Inputs:
    two text files with the bias and uncertainty for each model specified
    MODELNAME+'_'+BASIN+'_Bias.txt'
    MODELNAME+'_'+BASIN+'_Uncr.txt'
    
    pathtoadeck = '/Users/btrabing/Code/Data/' #path to a-deck data
    pathtoscheme = '/Users/btrabing/Code/' #path to bias and uncertainty tables
    MODELS = ['DSHP','LGEM'] #list of models to be used
    BASIN = 'AL' #specify the basin you want to run the models on

    
Developmental Switch:
    To run operationally set OPER to True, if testing, set OPER=False and set the datetime
    OPER=True
    OFF_Date = dt.datetime(2020,8,18,12).strftime('%Y%m%d%H')



Outputs a text file for each active storm with a recent 6 hour point (or specified datetime):



March 1, 2021
Benjamin Trabing, CIRA/CSU/NHC
Ben.Trabing@NOAA.gov
    
'''


###############################################
# Declaritives
#

pathtoadeck  = '../Data/AID/'
pathtoscheme = '../Input/'
MODELS       = ['DSHP','LGEM','HMNI','HWFI','CTCI','AVNI']
BASIN         = 'AL'

# If operationall run, set OPER to true
# If testing, set to False and specify datetime to run
# The IBUS will run on all TCs with a valid forecast on the OFF_Date
OPER=False
OFF_Date = dt.datetime(2021,8,22,0).strftime('%Y%m%d%H') # Provide YYYY MM DD HH

Output_Loc = '../Output/TXTs/'

###############################################
# Function Declarations


def BC_Values(BC_matrix,SD_matrix,FHOUR,FVCHANGE,XBIN,YBINL,YBINU):
    #Check if this is a zero hour forecast (i.e. no intensity change)
    if FHOUR==0:
        return 0,0
    
    #Check if data is transposed, if it was then transpose it back
    if len(XBIN)!=BC_matrix.shape[0]:
        BC_matrix = BC_matrix.T
        SD_matrix = SD_matrix.T
        

    try:
        INDEX = int(np.argwhere(str(FHOUR)==XBIN))
    except:
        raise Exception("Forecast Hour not in the Bias Correction")
        
    j=0       
    for vmin,vmax in zip(YBINL,YBINU):
        
        if (j==0)&(FVCHANGE<vmin):
            #If the change is less than the vmin bounds make it the edge
            BC_VCHANGE = BC_matrix[INDEX,j]
            SD_VCHANGE = SD_matrix[INDEX,j]
            return BC_VCHANGE,SD_VCHANGE    
        
        if (j==len(YBINL))&(FVCHANGE>vmax):
            #If the change is less than the vmin bounds make it the edge
            BC_VCHANGE = BC_matrix[INDEX,j]
            SD_VCHANGE = SD_matrix[INDEX,j]
            return BC_VCHANGE,SD_VCHANGE    
        
           
        if (FVCHANGE<vmax)&(FVCHANGE>vmin):
            BC_VCHANGE = BC_matrix[INDEX,j]
            SD_VCHANGE = SD_matrix[INDEX,j]
            return BC_VCHANGE,SD_VCHANGE
        

        j+=1

    
    raise Exception("Intensity Change not in the Bias Correction")
    

def run_IBUS(MODELNAMES,BASIN,pathtoa,pathtoscheme,Output_Loc,OPER=False):
    
    if OPER==True: #Option for realtime use
        #Get the current time 
        DTofAn = get_utctime() 
        
    elif OPER: #Option for development
        DTofAn = OPER
    
    YYYY =DTofAn[:4] #Isolate the Year of the storms


    # Loop through the Adecks and search for files that have been updated since most recent UTC time
    zz = 0
    for storm in sorted(glob.glob(pathtoa+'a'+BASIN.lower()+'[0-4][0-9]'+YYYY+'.dat')):

        if Config_.CONSENSUS==True:
            #We don't know how many models will be found, so use lists and add the data after found
            CON_MODELS  = []
            CON_FHOUR   = []
            CON_FINT    = []
            CON_FBIAS   = []
            CON_FSTDE   = []


        STORMNAME = storm[-12:-4]

        #Read in the A deck but only for the specified time
        Adata=remove_multi_afor(parseadeck(getadeck(STORMNAME,pathtoa,DTofAn)))

        
        if len(Adata)==0: #If there is an empty a-deck then skip
            continue
            print('No Recent Data Found for %s'%STORMNAME)
            
        else: #If there is something in the file then lets run the IBUS
            print('Generating Output for %s'%STORMNAME)
            
            #Output filename
            OUTPUT_NAME = Output_Loc+'/'+'IBUS_'+BASIN+'_'+STORMNAME+'_'+DTofAn+'.dat'
            
            #Open the file to start adding data
            with open(OUTPUT_NAME, 'w') as fp:
                
                #This will be the header of the file
                Header_ = [STORMNAME]+[' ']+[DTofAn]+[len(MODELNAMES)] + [Model for Model in MODELNAMES ] # Info about file
                Header = '\t'.join(["%s"%el for el in Header_]) #tab delimit header into right aligned columns
                fp.write(Header) #write to file
                fp.write('\n\n')   #Add space to next line

                
                #Now loop through models to add to output file
                ###################################
                for MODELNAME in MODELNAMES: #If there are multiple models to include the IBUS for
                    #Get the data for the analysis
                    Mdata = grabadeck(Adata,MODELNAME)
                    
                    #Not every model may have run, if the model was not run make that note
                    if len(Mdata)==0: #if there is no data here
                        #Output that No data exists
                        MRow0_ = ['MODEL']+[MODELNAME] + ['999']+ ['NOT'] + ['AVAIL']
                        MRow0 = '\t'.join([format("%s"%el, '>5') for el in MRow0_]) #tab delimit header into right aligned columns
                        fp.write(MRow0)  #write to file
                        fp.write('\n') #Move to next line
                        continue



                    #First read in the Schemes for the model
                    MODELNAME, BASIN, USCHEME, XBIN, YBINL, YBINU = read_IBUS(pathtoscheme+'%s_%s_Uncr.txt'%(MODELNAME,BASIN))    
                    MODELNAME, BASIN, BSCHEME, XBIN, YBINL, YBINU = read_IBUS(pathtoscheme+'%s_%s_Bias.txt'%(MODELNAME,BASIN))  

                    #Data to output in text file
                    #
                    #
                    # Data at initial time
                    O_IDTM = Mdata[0,0].strftime('%Y%m%d%H')
                    O_ILAT = Mdata[2,0]
                    O_ILON = Mdata[3,0]
                    O_IINT = Mdata[4,0]
                    
                    #Output the first line with the initial storm data
                    MRow0_ = ['MODEL']+[MODELNAME] + [O_ILAT] + [O_ILON] + [O_IINT]
                    MRow0 = '\t'.join([format("%s"%el, '>5') for el in MRow0_]) #tab delimit header into right aligned columns
                    fp.write(MRow0)  #write to file
                    fp.write('\n') #Move to next line


                    #Declare arrays for Forecast data with missing data set to 999
                    O_FHOUR = XBIN
                    O_FLAT  = np.full(len(XBIN),999,dtype=float)
                    O_FLON  = np.full(len(XBIN),999,dtype=float)
                    O_FINT  = np.full(len(XBIN),999,dtype=int)
                    O_FINTX = np.full(len(XBIN),999,dtype=int)
                    O_FBIAS = np.full(len(XBIN),999,dtype=int)
                    O_FSTDE = np.full(len(XBIN),999,dtype=int)
                    
                    ##########################
                    ii=0 #index of output array
                    i_dat = 0 #index of data array
                    for FHOUR,FVCHANGE in zip(Mdata[1,:],Mdata[6,:]):
                        #note that forecast hours were already sorted
                        if str(FHOUR)=='0':
                            # Data at initial time to apply to top line
                            O_IDTM = Mdata[0,i_dat].strftime('%Y%m%d%H')
                            O_ILAT = Mdata[2,i_dat]
                            O_ILON = Mdata[3,i_dat]
                            O_IINT = Mdata[4,i_dat]

                        elif str(FHOUR) in XBIN:
                            #If the forecast hour is one that is used in the scheme then apply IBUS
                            BiasFC,STDEFC = BC_Values(BSCHEME,USCHEME,FHOUR,FVCHANGE,XBIN,YBINL,YBINU)

                            if np.isnan(BiasFC):
                                BiasFC = 999
                            if np.isnan(STDEFC):
                                STDEFC = 999

                            O_FHOUR[ii]= "%03d" %int(FHOUR)
                            O_FLAT[ii] = "%.1f" %float(Mdata[2,i_dat])
                            O_FLON[ii] = "%.1f" %float(Mdata[3,i_dat])
                            O_FINT[ii] = "%.0f" %int(Mdata[4,i_dat])
                            O_FINTX[ii]= "%.0f" %int(FVCHANGE)
                            O_FBIAS[ii]= "%.0f" %float(BiasFC)
                            O_FSTDE[ii]= "%.0f" %float(STDEFC)


                            ii+=1

                        i_dat+=1
                    

                    #Combine in list to output in for loop
                    outputf = [O_FHOUR, O_FLAT, O_FLON, O_FINT, O_FINTX, O_FBIAS, O_FSTDE]
                    outputn = ['HOUR', 'FLAT', 'FLON', 'VMAX', 'DVDT', 'BIAS', 'UNCR']
                    
                    for outdat,oname in zip(outputf,outputn):
                        Row_ = [oname] + list(outdat)#add the lower and upper intensity change limits to row
                        Row = '\t'.join([format("%s"%el, '>5') for el in Row_]) #tab delimit header into right aligned columns
                        fp.write(Row)  #write to file
                        fp.write('\n') #Move to next line
                        
                    fp.write('\n') #After outputing all the data for the model, add space to separate

                    if Config_.CONSENSUS==True: #If true save the forecast model to lists
                        CON_MODELS.append(MODELNAME)
                        CON_FHOUR.append(O_FHOUR.astype(int))
                        CON_FINT.append(O_FINT.astype(float))
                        CON_FBIAS.append(O_FBIAS.astype(float))
                        CON_FSTDE.append(O_FSTDE.astype(float))                        
                    

                #--------------------CONSENSUS-----------------------#
                if Config_.CONSENSUS==True: #After all the models were found and already added, lets create a consensus
                    CON_INFO = Consensus_Config(BASIN) #Read Config file
                    CON_MODEL_TOINC = CON_INFO[0,CON_INFO[3]=='1'] #Which models to include


                    if len(CON_MODELS)<1:
                        print('No models available to include in consensus')
                        return

                    #Initialize arrays for forecast, corrected forecast, and the STDE
                    CON_FORECAST = np.empty((len(CON_MODEL_TOINC),len(XBIN)))
                    UNB_FORECAST = np.empty((len(CON_MODEL_TOINC),len(XBIN)))
                    CON_STDE     = np.empty((len(CON_MODEL_TOINC),len(XBIN)))

                    #Add nans to arrays
                    CON_FORECAST[:] = np.nan
                    UNB_FORECAST[:] = np.nan
                    CON_STDE[:]     = np.nan

                    INCL_CON_MODELS = []

                    for i,MODEL_ in enumerate(CON_MODELS): #Loop through models with data

                        if MODEL_ in CON_MODEL_TOINC: #Check if model should be in consensus
                            INCL_CON_MODELS.append(MODEL_)

                            CON_FINT[i][CON_FINT[i]==999]   =np.nan 
                            CON_FBIAS[i][CON_FBIAS[i]==999] =np.nan
                            CON_FSTDE[i][CON_FSTDE[i]==999] =np.nan

                            CON_FORECAST[i] = CON_FSTDE[i]*(CON_FINT[i]-CON_FBIAS[i])  
                            UNB_FORECAST[i] = CON_FINT[i]
                            CON_STDE[i]     = CON_FSTDE[i]

                    FUNB_FORECAST = np.nansum(UNB_FORECAST,axis=0)/np.count_nonzero(~np.isnan(UNB_FORECAST),axis=0)
                    FUNB_FORECAST = ["%.0f"%float(var) for var in np.nan_to_num(FUNB_FORECAST,999)]

                    NMODELS_INF   = np.count_nonzero(~np.isnan(UNB_FORECAST),axis=0)
                    NMODELS_INF   = ["%.0f"%float(var) for var in NMODELS_INF]

                    FCON_FORECAST = np.nansum(CON_FORECAST,axis=0)/np.nansum(CON_STDE,axis=0)
                    FCON_FORECAST = ["%.0f"%float(var) for var in np.nan_to_num(FCON_FORECAST,999)]
 
                    FCON_STDE     = np.nanmean(CON_STDE,axis=0)
                    FCON_STDE     = ["%.0f"%float(var) for var in np.nan_to_num(FCON_STDE,999)]



                    #Output the first line with the initial storm data
                    CRow0_ = ['CONSENSUS']+INCL_CON_MODELS 
                    CRow0 = '\t'.join([format("%s"%el, '>5') for el in CRow0_]) #tab delimit header into right aligned columns
                    fp.write(CRow0)  #write to file
                    fp.write('\n')
                    outputf = [O_FHOUR, NMODELS_INF,FUNB_FORECAST, FCON_FORECAST,FCON_STDE]
                    outputn = ['HOUR', 'NMOD', 'VMAX', 'CVMX', 'UNCR']

                    for outdat,oname in zip(outputf,outputn):
                        Row_ = [oname] + list(outdat)#add the data to the header as a list
                        Row = '\t'.join([format("%s"%el, '>5') for el in Row_]) #tab delimit header into right aligned columns
                        fp.write(Row)  #write to file
                        fp.write('\n') #Move to next line
                    


                
            print('Output Complete for %s'%STORMNAME)

 
if __name__ == "__main__":

    if OPER==True:
        run_IBUS(MODELS,BASIN,pathtoadeck,pathtoscheme,Output_Loc,OPER)
        
    elif OPER==False:
        run_IBUS(MODELS,BASIN,pathtoadeck,pathtoscheme,Output_Loc,OFF_Date)
     
    


