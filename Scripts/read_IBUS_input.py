#!/usr/bin/env python
###############################################
#
# Contains function to read in the IBUS input
#
###############################################
import numpy as np
import warnings
warnings.filterwarnings("ignore")

###############################################
# Declaritives
# Only used as an example here (use the function)

MODELNAME = 'HWFI'
BASIN ='AL'

###############################################
# Function Declarations

def read_IBUS(filename):
    '''
    Read in the IBUS file and should be applied to both uncertainty and bias file
    
    
    '''
    
    with open(filename, 'r') as fp: #open file
        
        DATA = [] #define empty list
        YBINL = [] #define empty list
        YBINU = [] #define empty list
        
        for ind,line in enumerate(fp): #loop through each line
            #Each line will have the same length, so no special logic needed here
            data_line = np.array([x.strip(' ').rstrip() for x in line.split('\t')]) #Strip line to list of strings
            
            if ind==0: #If this is the first line (i.e. header)
                MODELNAME = data_line[0] #Model name
                BASIN = data_line[1] # Basin ID
                XBIN = data_line[2:] #This is the forecast hours
                
            else:
                DATA.append(data_line[2:].astype(np.float)) #add the data to the list
                YBINL.append(np.float(data_line[0])) #the 1st value in each row is the lower intensity change bound
                YBINU.append(np.float(data_line[1])) #the 2nd value in each row is the upper intensity change bound
                
                
    #Return the model name, the basin, the scheme data, the forecast hour bin, and the intensity change values as arrays
    return MODELNAME,BASIN,np.array(DATA),XBIN,np.array(YBINL),np.array(YBINU)

if __name__ == "__main__":
    
    MODELNAME, BASIN, USCHEME, XBIN, YBINL, YBINU = read_IBUS('../Input/%s_%s_Uncr.txt'%(MODELNAME,BASIN))    
    MODELNAME, BASIN, BSCHEME, XBIN, YBINL, YBINU = read_IBUS('../Input/%s_%s_Bias.txt'%(MODELNAME,BASIN))    



