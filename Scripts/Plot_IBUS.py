#!/usr/bin/env python
###############################################
import datetime as dt
import glob
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.ticker import AutoMinorLocator

from IBUS_util import *
from read_IBUS_input import *
###############################################
'''

This code will make plots of the IBUS output for multiple models and for specific models
  

March 1, 2021
Benjamin Trabing, CIRA/CSU/NHC
Ben.Trabing@NOAA.gov
    
'''


#############################################
# Declaritives
#

pathtoIBUS = '../Output/TXTs/'
MODELS = Config_.Model_List  #['DSHP','LGEM','HMNI','HWFI','AVNI','CTCI']
BASIN = 'AL'

#If operationall fun, set OPER to true
#If testing, set to False and specify datetime to run
OPER=False
OFF_Date = dt.datetime(2021,8,22,0).strftime('%Y%m%d%H')

Output_Loc = '../Output/Figs/'

###############################################
# Function Declarations
def mdict():
    modelc = np.genfromtxt('../Config/model_cparams.txt',dtype=str,delimiter=',',skip_header=1)
    markers=['*','D', '+', 'v',4,5,6,7,8,9, '.', 'o', ' ','x','^', 's', 'p', 'h', ',',]


    mcdict = {}
    mmdict = {}
    for line in modelc[:]:
        mcdict[line[0]]=line[1].strip()
        mmdict[line[0]]=markers[int(line[3].strip())]

    return mcdict,mmdict




def catshade(IBUS):
    #
    # This funtion takes in IBUS data for a model, but only creates the figure and adds shading for intensity
    #
    #
        
    x = [0]+list(IBUS[0].astype(np.float)) #Because the ibus does not have zero, we add it
    
    fig = plt.figure(figsize=(10,5))
    ax = plt.subplot(111)
    
    ax.tick_params(axis='y',labelsize=12)
    ax.tick_params(axis='x',labelsize=12)

    ax.fill_between(np.arange(-24,192,24),64,83,color='silver',zorder=1,alpha=.3)
    ax.fill_between(np.arange(-24,192,24),96,113,color='silver',zorder=1,alpha=.3)
    ax.fill_between(np.arange(-24,192,24),137,190,color='silver',zorder=1,alpha=.3)
    
    ax.set_xlim(0,x[-1]) #xlim is set to 0 to 120 (can be 168 if provided)
    ax.set_ylim(15,160) #Current max intensity shown is 160 kt
    
    
    ax.set_xticks(np.arange(-12,np.nanmax(x),12))
    minor_locator = AutoMinorLocator(4)
    ax.xaxis.set_minor_locator(minor_locator)
    

    ax.set_xlabel('Forecast Hour', fontsize=16)    
    ax.set_ylabel('Intensity (kt)', fontsize=16)
    
    return fig, ax
    


def plot_all_IBUS(BASIN,pathtoIBUS,Output_Loc,OPER=False):
    #
    # This code will plot all the storms that have available model runs at the specified time
    #
    #
    #
    
    if OPER==True: #Option for realtime use
        #Get the current time 
        DTofAn = get_utctime() 
        
    elif OPER: #Option for development
        DTofAn = OPER
    
    YYYY =DTofAn[:4] #Isolate the Year of the storms

    OUTPUT_NAME = 'IBUS_'+BASIN+'_*'+'_'+DTofAn+'.dat'
    
    mcdict,mmdict=mdict() #Get colors and markers for models


    # Loop through the Adecks and search for files that have been updated since most recent UTC time
    for filename in sorted(glob.glob(pathtoIBUS+OUTPUT_NAME)):
        STORMNAME=filename.split('_')[-2]
        
        #Read in the IBUS file with the specified filename
        Data,LAT, Lon, VMX,MODELS = read_IBUS_output(filename)

        if Data.shape[0]<1:
            continue
        
        for ModelN in range(0,Data.shape[0]):
            
            #If this is the first plot use catshade to initialize the figure and shade the intensities.
            if ModelN==0:
                fig, ax = catshade(Data[ModelN])
                ax.plot([0]+list(Data[ModelN][0]),[VMX[ModelN]]+list(Data[ModelN][3]-Data[ModelN][5]),c=mcdict[MODELS[ModelN]],lw=3,markersize=8,marker=mmdict[MODELS[ModelN]],label=MODELS[ModelN],zorder=99)
                ax.fill_between([0]+list(Data[ModelN][0]),[VMX[ModelN]]+list(Data[ModelN][3]-Data[ModelN][5]-Data[ModelN][6]),[VMX[ModelN]]+list(Data[ModelN][3]-Data[ModelN][5]+Data[ModelN][6]),color=mcdict[MODELS[ModelN]],alpha=.2)
                

            else:
                ax.plot([0]+list(Data[ModelN][0]),[VMX[ModelN]]+list(Data[ModelN][3]-Data[ModelN][5]),c=mcdict[MODELS[ModelN]],lw=3,markersize=8,marker=mmdict[MODELS[ModelN]],label=MODELS[ModelN],zorder=99)
                ax.fill_between([0]+list(Data[ModelN][0]),[VMX[ModelN]]+list(Data[ModelN][3]-Data[ModelN][5]-Data[ModelN][6]),[VMX[ModelN]]+list(Data[ModelN][3]-Data[ModelN][5]+Data[ModelN][6]),color=mcdict[MODELS[ModelN]],alpha=.2)


        #Add a legend for each plot
        props=dict(facecolor='w',alpha=.9,edgecolor='k')
        ax.legend(frameon=True,prop={'size':14},loc=0)
        
        #Plot the storm name and the initialization time of the models
        props2=dict(facecolor='white',alpha=.9,edgecolor='None')
        ax.text(.05,1.02,STORMNAME.upper(),fontsize=16,color='k', bbox=props2,ha="left",va='bottom',transform=ax.transAxes,zorder=99999)
        ax.text(.95,1.02,'Init: %s'%dt.datetime.strptime(DTofAn,'%Y%m%d%H').strftime('%HZ %b %d'),fontsize=16,color='k', bbox=props2,ha="right",va='bottom',transform=ax.transAxes,zorder=99999)

        #Show the plot
        plt.savefig((Output_Loc+'IBUSP_'+STORMNAME+'_'+BASIN+'_'+DTofAn+'.png'),bbox_inches='tight')

        plt.close(fig)    
#        plt.show()
                
        

def plot_MODEL_IBUS(BASIN,pathtoIBUS,MODELNAME,Output_Loc,OPER=False):
    #
    # This code will plot all the storms that have IBUS output at the specified time
    #
    #
    #
    
    if OPER==True: #Option for realtime use
        #Get the current time 
        DTofAn = get_utctime() 
        
    elif OPER: #Option for development
        DTofAn = OPER
    
    YYYY =DTofAn[:4] #Isolate the Year of the storms

    OUTPUT_NAME = 'IBUS_'+BASIN+'_*'+'_'+DTofAn+'.dat'
    
    mcdict,mmdict=mdict() #Get colors and markers for models


    # Loop through the IBUS files with given time (could have multiple storms
    for filename in sorted(glob.glob(pathtoIBUS+OUTPUT_NAME)):
        STORMNAME=filename.split('_')[-2]
        
        #Read in the IBUS file with the specified filename
        Data,LAT, Lon, VMX,MODELS = read_IBUS_output(filename)
        
        if MODELNAME in list(MODELS):
            print('Making figure for %s forecast at %s'%(MODELNAME,dt.datetime.strptime(DTofAn,'%Y%m%d%H').strftime('%HZ %b %d')))
        
        else:
            #If there is no model forecast in the file, then don't do anything
            continue 
        
        for ModelN in range(0,Data.shape[0]):
                        
            #If this is the first plot use catshade to initialize the figure and shade the intensities.
            if MODELS[ModelN]==MODELNAME:
                fig,ax = catshade(Data[ModelN])
                ax.plot([0]+list(Data[ModelN][0]),[VMX[ModelN]]+list(Data[ModelN][3]-Data[ModelN][5]),c=mcdict[MODELS[ModelN]],lw=3,markersize=10,marker=mmdict[MODELS[ModelN]],label=MODELS[ModelN],zorder=9999)
                ax.plot([0]+list(Data[ModelN][0]),[VMX[ModelN]]+list(Data[ModelN][3]-Data[ModelN][5]+2.*Data[ModelN][6]),c=mcdict[MODELS[ModelN]],lw=3,markersize=8,ls='dotted')
                ax.plot([0]+list(Data[ModelN][0]),[VMX[ModelN]]+list(Data[ModelN][3]-Data[ModelN][5]-2.*Data[ModelN][6]),c=mcdict[MODELS[ModelN]],lw=3,markersize=8,ls='dotted')
                ax.fill_between([0]+list(Data[ModelN][0]),[VMX[ModelN]]+list(Data[ModelN][3]-Data[ModelN][5]-Data[ModelN][6]),[VMX[ModelN]]+list(Data[ModelN][3]-Data[ModelN][5]+Data[ModelN][6]),color=mcdict[MODELS[ModelN]],alpha=.5)


        #Add a legend for each plot
        props=dict(facecolor='w',alpha=.9,edgecolor='k')
        ax.legend(frameon=True,prop={'size':14},loc=0)
        
        #Plot the storm name and the initialization time of the models
        props2=dict(facecolor='white',alpha=.9,edgecolor='None')
        ax.text(.05,1.02,STORMNAME.upper(),fontsize=16,color='k', bbox=props2,ha="left",va='bottom',transform=ax.transAxes,zorder=99999)
        ax.text(.95,1.02,'Init: %s'%dt.datetime.strptime(DTofAn,'%Y%m%d%H').strftime('%HZ %b %d'),fontsize=16,color='k', bbox=props2,ha="right",va='bottom',transform=ax.transAxes,zorder=99999)

        #Show the plot    
        plt.savefig((Output_Loc+'IBUSP_'+STORMNAME+'_'+BASIN+'_'+DTofAn+'.png'),bbox_inches='tight')

        plt.close(fig)
                      

def plot_MODEL_IBUS_P(BASIN,pathtoIBUS,MODELNAME,Output_Loc,OPER=False):
    #
    # This code will plot all the storms that have IBUS output at the specified time
    #
    #
    #

    if OPER==True: #Option for realtime use
        #Get the current time 
        DTofAn = get_utctime()

    elif OPER: #Option for development
        DTofAn = OPER

    YYYY =DTofAn[:4] #Isolate the Year of the storms



    OUTPUT_NAME = 'IBUS_'+BASIN+'_*'+'_'+DTofAn+'.dat'


    mcdict,mmdict=mdict() #Get colors and markers for models


    # Loop through the IBUS files with given time (could have multiple storms
    for filename in sorted(glob.glob(pathtoIBUS+OUTPUT_NAME)):
        STORMNAME=filename.split('_')[-2]

        
        #Read in the IBUS file with the specified filename
        Data,LAT, Lon, VMX,MODELS = read_IBUS_output(filename)

        if MODELNAME in list(MODELS):
            print('Making figure for %s forecast at %s'%(MODELNAME,dt.datetime.strptime(DTofAn,'%Y%m%d%H').strftime('%HZ %b %d')))

        else:
            #If there is no model forecast in the file, then don't do anything
            continue

        for ModelN in range(0,Data.shape[0]):

            #If this is the first plot use catshade to initialize the figure and shade the intensities.
            if MODELS[ModelN]==MODELNAME:
                fig,ax = catshade(Data[ModelN])

                ax.axvline(0,c='k',ls='dotted',lw=2)
                ax.plot([0]+list(Data[ModelN][0]),[VMX[ModelN]]+list(Data[ModelN][3]-Data[ModelN][5]),c=mcdict[MODELS[ModelN]],lw=3,markersize=10,marker=mmdict[MODELS[ModelN]],label=MODELS[ModelN],zorder=9999)
                ax.fill_between([0]+list(Data[ModelN][0]),[VMX[ModelN]]+list(Data[ModelN][3]-Data[ModelN][5]-Data[ModelN][6]),[VMX[ModelN]]+list(Data[ModelN][3]-Data[ModelN][5]+Data[ModelN][6]),color=mcdict[MODELS[ModelN]],alpha=.5)


        #######
        #Now we should try to find older files
        DTofAn_dt = dt.datetime.strptime(DTofAn,'%Y%m%d%H')
        
        alphas = [.2,.4,.7]
        marks_ = ['$18h$','$12h$','$6h$']                

        for indx,prvioush in enumerate([18,12,6]): #Look for the 18, 12, and 6 hour files in that order
            alpha_=alphas[indx]
            marker_ = marks_[indx]
            newtimestr = (DTofAn_dt - dt.timedelta(hours=prvioush)).strftime('%Y%m%d%H')
            newfilename = filename.replace(DTofAn,newtimestr)

            try:
                Data_,LAT_, Lon_, VMX_,MODELS_ = read_IBUS_output(newfilename) #Try to pull the data, if there is an error continue to next time
            except:
                continue

            if MODELNAME not in list(MODELS_): #Now that we have data, check if that model is actually available
                continue


            for ModelN_ in range(0,Data_.shape[0]): #Iterate through list of models
                if MODELS_[ModelN_]==MODELNAME: #If we found the model, then lets plot it
                    ax.axvline(-prvioush,c=mcdict[MODELS_[ModelN_]],lw=2,ls='dashed',alpha=alpha_)
                    ax.plot(np.array([0]+list(Data_[ModelN_][0]))-prvioush,[VMX_[ModelN_]]+list(Data_[ModelN_][3]-Data_[ModelN_][5]),c=mcdict[MODELS_[ModelN_]],lw=2,markersize=12,alpha=alpha_,marker=marker_,zorder=(24-prvioush))

        ax.set_xlim(-19,120) #set limits for plot so show older data start times

        #Add a legend for each plot
        props=dict(facecolor='w',alpha=.9,edgecolor='k')
        ax.legend(frameon=True,prop={'size':14},loc=0)

        #Plot the storm name and the initialization time of the models
        props2=dict(facecolor='white',alpha=.9,edgecolor='None')
        ax.text(.05,1.02,STORMNAME.upper(),fontsize=16,color='k', bbox=props2,ha="left",va='bottom',transform=ax.transAxes,zorder=99999)
        ax.text(.95,1.02,'Init: %s'%dt.datetime.strptime(DTofAn,'%Y%m%d%H').strftime('%HZ %b %d'),fontsize=16,color='k', bbox=props2,ha="right",va='bottom',transform=ax.transAxes,zorder=99999)

        #Show the plot    
        plt.savefig((Output_Loc+'MODEL_IBUS_'+MODELNAME+'_'+STORMNAME+'_'+BASIN+'_'+DTofAn+'.png'),bbox_inches='tight')

        plt.close(fig)

 
if __name__ == "__main__":

    if OPER==True:
        plot_all_IBUS(BASIN,pathtoIBUS,Output_Loc,OPER)
        
    elif OPER==False:
        for MODEL in MODELS:
            plot_MODEL_IBUS_P(BASIN,pathtoIBUS,MODEL,Output_Loc,OFF_Date)
     
    



