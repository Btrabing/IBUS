#!/usr/bin/env python
####################################################
#
# Creates an on the fly verification of the IBUS input
# applied to all forecasts in the specified years
#
#
#
#
####################################################
import subprocess
import numpy as np
import glob
import datetime as dt
import sys
import math
from matplotlib import pyplot as plt
from scipy.stats import stats
import warnings
warnings.filterwarnings("ignore")
####################################################

from read_IBUS_input import *
from Eval_util import *
from IBUS_util import *
from Plot_IBUS import *
from IBUS_main import *
        
#--------------------------------------------------------
Path_to_adecks = '../Data/AID/'
Path_to_bdecks = '../Data/BTK/'
Path_to_input = '../Input/'

Path_to_output = '../Eval/'

MODELS = Config_.Model_List  # Or specify with a list ['HWFI'] 

BASIN = 'AL'
YYYY = ['2021'] #or list of start end year [2020,2022]

EVAL_CONSENSUS    = True
PLOT_ALL_TOGETHER = True

#--------------------------------------------------------
if EVAL_CONSENSUS:
    SNdata, Sdata,SBdata,SDdata,SD_data,SER_data,STORM_NAMES = CONSENSUS_Year_Verif_BC(YYYY,BASIN,Path_to_adecks,Path_to_bdecks,Path_to_input)

    plot_corr(SD_data,SER_data,SNdata, BASIN, 'CONSENSUS', YYYY,Path_to_output)
    plot_maeskill(Sdata,SBdata,SNdata, BASIN, 'CONSENSUS', YYYY,Path_to_output)


if PLOT_ALL_TOGETHER:
    F_name = np.arange(Config_.F_START,Config_.F_END+Config_.F_INTER,Config_.F_INTER).astype(float)

    ii=0

    fig = plt.figure(figsize=(9,15))
    ax1 = plt.subplot(311)
    ax2 = plt.subplot(312)
    ax3 = plt.subplot(313)    
    
    mcdict,mmdict=mdict() #Get colors and markers for models


    

for MODEL in MODELS:
    SNdata, Sdata,SBdata,SDdata,SD_data,SER_data,STORM_NAMES,SLATS,SLONS = get_Year_verif_BC(YYYY,BASIN,MODEL,Path_to_adecks,Path_to_bdecks,Path_to_input)

    
    plot_corr(SD_data,SER_data,SNdata, BASIN, MODEL, YYYY,Path_to_output)

    plot_maeskill(Sdata,SBdata,SNdata, BASIN, MODEL, YYYY,Path_to_output)
        
    sbs_MAE(Sdata,SBdata,STORM_NAMES, BASIN, MODEL, YYYY,Path_to_output)
    
    if PLOT_ALL_TOGETHER:

        #Positive skill means that there is improvement
        SKILL_Diff = skill(np.nansum(Sdata*SNdata,axis=0)/np.sum(SNdata,axis=0), np.nansum(SBdata*SNdata,axis=0)/np.sum(SNdata,axis=0))

        # % variance explained
        VAR_EXP = (np.array(100.*get_cor_p(SD_data,SER_data)[0]**2).astype(float))

        SAMP = np.sum(SNdata,axis=0)

        ax1.plot(F_name,SKILL_Diff,c=mcdict[MODEL],lw=3,markersize=8,marker=mmdict[MODEL],label=MODEL)

    
        ax2.plot(F_name,VAR_EXP,c=mcdict[MODEL],lw=3,markersize=8,marker=mmdict[MODEL],label=MODEL)     

  
        ax3.bar(F_name-float(Config_.F_INTER)/2.+(ii*(float(Config_.F_INTER)/len(MODELS))),SAMP,(float(Config_.F_INTER)/len(MODELS)),facecolor=mcdict[MODEL],label=MODEL)    


        ii+=1


                

if PLOT_ALL_TOGETHER:
    cutedge(ax1)
    cutedge(ax2)
    cutedge(ax3)
                
    ax1.set_xticks(F_name)
    ax2.set_xticks(F_name)
    ax3.set_xticks(F_name)
                
    ax2.set_ylim(0,60)
                
    ax1.axhline(0,c='k',ls='dotted',lw=2,zorder=-1)

    props2=dict(facecolor='white',alpha=.9,edgecolor='none')

    ax1.text(.03,1.02,BASIN,fontsize=16,color='k', bbox=props2,ha="left",va='bottom',transform=ax1.transAxes)
    ax1.text(.97,1.02,'-'.join(YYYY),fontsize=16,color='k', bbox=props2,ha="right",va='bottom',transform=ax1.transAxes)

    ax1.set_ylabel('Skill Difference (%)',fontsize=14)
    ax2.set_ylabel('Variance Explained (%)',fontsize=14)    
    ax3.set_ylabel('Sample Size ',fontsize=14)

    ax1.set_xlabel('Forecast Hour ',fontsize=14)
    ax2.set_xlabel('Forecast Hour ',fontsize=14)
    ax3.set_xlabel('Forecast Hour ',fontsize=14)

    
    ax3.legend()
    ax2.legend(loc=1)
    ax1.legend(loc=1)
    
    plt.savefig(Path_to_output+'/'+'ALLCASE_%s_%s_SKILL_VAR_SAMP.pdf'%(BASIN,'_'.join(YYYY)),bbox_inches='tight')
    plt.close(fig)
#    plt.show()


