###############################################

import datetime as dt
import glob
import numpy as np
from scipy.interpolate import interp1d
import pandas as pd
import xarray as xr

###############################################

class Config_:
    #To call config variables using this class use
    #   Config_."VARNAME"
    # Example: to get model list call Config_.Model_List

    #Read in the namelist file
    config_file = np.genfromtxt('../Config/NAMELIST', dtype=str)

    for line in config_file: #Go through the file and execute the commands
         exec(''.join(line)) #What this does is essentially creat a global variable that is found when importing this code


def Consensus_Config(BASIN):
    #Outputs the config file for the consensus
    #requires input of the basin name as in "AL" for Atlantic

    MODELNAME = []
    MODELBIAS = []
    MODELUNCT = []
    MODELCONS = []

    with open('../Config/%s_Consensus.txt'%BASIN,'r') as fp:
        for i,line in enumerate(fp):
            if i==0:
                #Header
                continue

            data = [val.strip(' \t\n\r') for val in line.split(',')]
            MODELNAME.append(data[0])
            MODELBIAS.append(data[1])
            MODELUNCT.append(data[2])
            MODELCONS.append(data[3])
    
    return np.squeeze((MODELNAME, MODELBIAS, MODELUNCT, MODELCONS))



def get_utctime():
    '''
    Returns the most recent utc time at the 6 hour mark
    
    #Requires
    import datetime as dt
    import numpy as np
    '''
    #Get current time rounded down to nearest hour
    current = dt.datetime.utcnow().replace(microsecond=0, second=0, minute=0)

    #Declare the 4 potential utc 6 hour intervals to choose from 
    ptimes = np.array([current.replace(hour=0),current.replace(hour=6),current.replace(hour=12),current.replace(hour=18)])
    
    return ptimes[ptimes<=current].max().strftime('%Y%m%d%H') #Return the most recent utc 6 hour time


def get_alltime(dt_start,dt_end):
    '''
    Returns 6 hourly times from start to end
    
    #Requires
    import datetime as dt
    import numpy as np
    '''
    #Declare the 4 potential utc 6 hour intervals to choose from 
    start_time = dt_start.replace(hour=0)
    end_time   = dt_end.replace(hour=18)

    TIME_LIST = []

    while (end_time-start_time).total_seconds()>=0:
        TIME_LIST.append(start_time.strftime('%Y%m%d%H'))
        start_time= start_time+dt.timedelta(hours=6)

    return TIME_LIST #Return the list of utc 6 hourly times



def getadeck(STORMNAME,pathtoa,DTofAn):
#    print('Reading in A-deck for '+STORMNAME)
    
    #Read in the file
    with open(pathtoa+'a'+STORMNAME.lower()+'.dat') as f:
        content = []
        for line in f:
            content.append(line.split(','))

    newcontent = []
    for i,line in enumerate(content): 
        if (line[2].strip() == DTofAn):
            newcontent.append(line[:20]) #Takes only the first 20 variables for consistency

    return np.array(newcontent)
    
    
def parseadeck(data):
    # Parses the a deck format and converts to numpy arrays
    # Only outputs the datetime, forecast hour, lat, lon, vmax, and model name

    #If the file is empty return empty list
    if len(data)==0:
        return []

    lat = []
    lon = []
    dti = []
    vmax = []
    names = []
    ff = []

    for row in data:
        dti.append(dt.datetime.strptime(row[2].strip(),'%Y%m%d%H'))
        lat.append(float(row[6][:-1])/10.)
        lon.append(-float(row[7][:-1])/10.)
        vmax.append(float(row[8]))
        names.append(row[4].strip())
        ff.append(int(row[5]))
    
    return np.array(dti),np.array(ff),np.array(lat),np.array(lon),np.array(vmax),np.array(names)
    

def grabadeck(adata,model):
    DATA = np.squeeze(np.array(adata)) #convert to numpy array and clear nested arrays

    mDATA = np.squeeze(np.sort(DATA[:,np.where(DATA[5]==model)],axis=1)) #get data for the specified model only

    #Check if there is forecast data for this model
    if mDATA.size == 0:
        return [] #if there is no data return empty list
    
    else:
        mDATA = np.vstack([mDATA, (mDATA[4,:]-mDATA[4,0])]) #Add vmax change to end of array

        #Return array with index datetime, Fhour, Lat, Lon, VMAX, name, Vmax_change
        return mDATA



def get_best(file_path,dttoref=None):
    '''
    This code will grab all the best track data that are available in the bdeck file
    The output will be the formatted data for the storm

    input atcf path with storm name attached to end
    input single datetime string to put into hours since

    output numpy arrays of
    datetime YYYYMMDDHH as a string
    forecast hour as int
    lat in deg as float
    lon in deg as float
    intensity in kt as int
   '''

    dtg_ALL      = []
    fhr_ALL      = []
    lat_ALL      = []
    lon_ALL      = []
    int_ALL      = []

    #This will pull all the forecast data and remove any 50 or 64 kt wind duplicates
    data_list        = str(subprocess.check_output(("grep " + "BEST "+file_path+" | awk -F', ' '{ if(substr($12,length($12)-1,2)!=\"50\" && substr($12,length($12)-1,2)!=\"64\") print}' | sort -u"),shell=True).decode('utf8')).splitlines()

    for data in data_list:
        line = data.split(',')
        #Add the data to the empty list
        dtg_ALL.append(line[2].strip(' '))
        fhr_ALL.append(int(line[5].strip(' ')))

        #Check which hemisphere the latitude is to format as a float
        if line[6].strip(' ')[-1]=='N':
            lat_ALL.append(float(line[6].strip(' ')[:-1])/10.)
        elif line[6].strip(' ')[-1]=='S':
            lat_ALL.append(-float(line[6].strip(' ')[:-1])/10.)
        else:
            lat_ALL.append(np.nan)

        #Now check which hemisphere the longitude is to format as float
        if line[7].strip(' ')[-1]=='E':
            lon_ALL.append(float(line[7].strip(' ')[:-1])/10.)
        elif line[7].strip(' ')[-1]=='W':
            lon_ALL.append(-float(line[7].strip(' ')[:-1])/10.)
        else:
            lon_ALL.append(np.nan)

        int_ALL.append(int(line[8].strip(' ')))
        
    dtg_ALL     = np.array(dtg_ALL)
    fhr_ALL     = np.array(fhr_ALL)
    lat_ALL     = np.array(lat_ALL)
    lon_ALL     = np.array(lon_ALL)
    int_ALL     = np.array(int_ALL)
    
    if dttoref!=None:
        init_dt = dt.datetime.strptime(dttoref,'%Y%m%d%H')

        for idx,dtg_ in enumerate(dtg_ALL):
            dtg_v = dt.datetime.strptime(dtg_,'%Y%m%d%H')
            fhr_ALL[idx] = int((dtg_v-init_dt).total_seconds()/(60.*60.))
 

    return dtg_ALL, fhr_ALL, lat_ALL, lon_ALL, int_ALL



def remove_multi_afor(var):
    #This removal is for simplified data files after parsing that has only  dti, fh, lat, lon, vmax, and modelname
    #Check if variable is empty, return empty
    if len(var)==0:
        return []

    dti=var[1]
    todel = []
    for i,dti_ in enumerate(dti):
        if i==0:
            continue

        if dti[i-1] == dti[i]:
            todel.append(i)

    varlist = []
    for v in var:
        varlist.append(np.delete(v,todel))

    return np.array(varlist)



def read_IBUS_output(filename):
    #This file can be unique in shape with each run depending on model availability
    
    iid=0 
    
    with open(filename, 'r') as fp: #open file

        TotalDATA = [] #define empty list for data
        TotalDATAX = [] #define empty list for row names            
        MODELNAMESS = [] #define empty list of model names
        InitLAT = [] 
        InitLON = [] 
        InitVMX = [] 

        #Loop through each line
        for ind,line in enumerate(fp): #loop through each line
            data_line = np.array([x.strip(' ').rstrip() for x in line.split('\t')]) #Strip line to list of strings

            #The first line is a header and tells which models were attempted to include
            if ind==0: #If this is the first line (i.e. header)
                STORMNAME=data_line[0]
                DATETIME = data_line[2]

            ##If there is only empty space, then skip    
            if len(data_line)<=1:
                continue

            #If MODEL is found, then we know that is a header line for the model initial conditions
            if data_line[0]=='MODEL':
                MODEL = data_line[1] #Second column in row is the name of the model
                
                #Make sure the data is in the file
                if data_line[2]=='999': #if the third column is 999 then the data was not present: skip
                    iid=0
                    continue

                else:  
                    #save the data
                    MODELNAMESS.append(MODEL)
                    InitLAT.append(np.float(data_line[2]))
                    InitLON.append(np.float(data_line[3]))
                    InitVMX.append(np.float(data_line[4]))

                    DATA = [] #ininialize data list for the model
                    DATAX =[] #list of variables in the file

                    iid=1 #line ID
                    continue

            #Now save the rest of the lines to the new data list
            elif (iid==1)&(data_line[0]!='UNCR'): #there are 7 lines of data
                variable = np.array(data_line[1:]).astype(np.float)
                variable[variable==999.]=np.nan
                DATA.append(variable)
                DATAX.append(data_line[0])

            #If you get to the last line of data which is 'UNCR' then
            elif (iid==1)&(data_line[0]=='UNCR'):
                variable = np.array(data_line[1:]).astype(np.float)
                variable[variable==999.]=np.nan
                DATA.append(variable)
                DATAX.append(data_line[0])

                iid=0

                #This is the last line, so add all the data to list
                TotalDATA.append(np.squeeze(np.array(DATA)))
                TotalDATAX.append(np.squeeze(np.array(DATAX)))



        #output the data as an np.array
        return np.squeeze(TotalDATA), InitLAT, InitLON, InitVMX, MODELNAMESS



def get_IBUS_Consensus(filename):
    #This file can be unique in shape with each run depending on model availability

    iid=0

    with open(filename, 'r') as fp: #open file

        #Loop through each line
        for ind,line in enumerate(fp): #loop through each line
            data_line = np.array([x.strip(' ').rstrip() for x in line.split('\t')]) #Strip line to list of strings

            #The first line is a header and tells which models were attempted to include
            if ind==0: #If this is the first line (i.e. header)
                STORMNAME=data_line[0]
                DATETIME = data_line[2]

            ##If there is only empty space, then skip    
            if len(data_line)<=1:
                continue

            #If MODEL is found, then we know that is a header line for the model initial conditions
            if data_line[0]=='CONSENSUS':

                DATA = [] #ininialize data list for the model
                DATAX =[] #list of variables in the file

                iid=1 #line ID
                continue

            #Now save the rest of the lines to the new data list
            elif (iid==1)&(data_line[0]!='UNCR'): #there are 4 lines of data
                variable = np.array(data_line[1:]).astype(np.float)
                variable[variable==999.]=np.nan
                DATA.append(variable)
                DATAX.append(data_line[0])

            #If you get to the last line of data which is 'UNCR' then
            elif (iid==1)&(data_line[0]=='UNCR'):
                variable = np.array(data_line[1:]).astype(np.float)
                variable[variable==999.]=np.nan
                DATA.append(variable)
                DATAX.append(data_line[0])

                
                #output the data as an np.array
                return np.squeeze(DATA),np.squeeze(DATAX) 





def interp_linear(x,y,x2):
    f2 = interp1d(x, y, kind='linear')
    return f2(x2)


#------------------#
def read_gdland(filepath):
    #Read the gdland file (which has the distance to land points)

    with open(filepath) as f:
        header = next(f)
        data = pd.read_fwf(f,header=None, widths=[9,]*10)

    coords = header.split()
    lons = np.linspace(float(coords[0]), float(coords[1]), int(coords[3]))
    lats = np.linspace(float(coords[4]), float(coords[5]), int(coords[7]), endpoint=False)
    gdland = xr.DataArray(data=np.array(data).reshape(len(lats),len(lons)),
                coords={'lat':lats, 'lon':lons}, dims=('lat','lon'))

    gdland = gdland.fillna(0)
    return gdland

#------------------#
def check_if_land(times,lon,lat,landfile=None):
    #Identifies whether the provided lat, lon is overland
    

    if landfile is None:
        #Read in gdland file if not provided
        #Use the expected location in the Data directory
        gdland = read_gdland('../Data/gdland_table.dat') #Inefficient if calling thousands of time
    else:
        #Assume that the landfile is being provided
        gdland = landfile

    #Get lat/lon seperate
    lons = gdland['lon'].values
    lats = gdland['lat'].values

    d2l = gdland.values #get the distance to land variables


    #If an array of lons are provided then lets calculate the dtl for each point
    if len(lon)>1:
        #Lets interpolate
        nlon = interp_linear(times,lon,np.arange(times[0],times[-1]+1,1))
        nlat = interp_linear(times,lat,np.arange(times[0],times[-1]+1,1))


        d2ls=[]
        i=0
        for lat_,lon_ in zip(nlat,nlon):
            d2ls.append(d2l[np.argmin(np.abs(lats-lat_)),np.argmin(np.abs(lons-lon_))])
        return np.array(d2ls) #return the distance to land 

    else:
        #Return the nearest defined distance to land point to the given lat/lon
        return d2l[np.argmin(np.abs(lats-lat)),np.argmin(np.abs(lons-lon))]


