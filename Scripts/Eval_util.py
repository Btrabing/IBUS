#!/usr/bin/env python
#########################################
import math
import numpy as np
from matplotlib import pyplot as plt
import datetime as dt
from scipy.stats import stats
from scipy.stats.stats import pearsonr
import glob
import subprocess
##########################################
from read_IBUS_input import *
from Plot_IBUS import *
from IBUS_util import *
from IBUS_main import *
##########################################


def myround(x, base=5):
    return base * np.round(x/base)

def roundD(x, base=5):
    return base * np.floor(x/base)

def roundU(x, base=5):
    return base * np.ceil(x/base)

def cutedge(ax):
    ax.tick_params(axis='y',labelsize=12)
    ax.tick_params(axis='x',labelsize=12)
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    
def skill(smerr,err):
    return 100*(smerr-err)/smerr



def verif_ic_wbc(Model,Adata,Bdata,BC_Matrix=False,SD_Matrix=False):
    import datetime as dt
    #For ADECK
    #BASIN     = 0
    #STORM#    = 1
    #DATETIME  = 2
    #UNKNOWN   = 3
    #MODEL     = 4
    #Forec Hour= 5
    #Forec Lat = 6
    #Forec Lon = 7
    #Forec INT = 8
    #
    #Stormtype = 10
    
    Verifying_Hours = np.array(['12','24','36','48','60','72','84','96','108','120','132','144','156','168']).astype(float)
    Verifying_Hours = np.arange(Config_.F_START,Config_.F_END+Config_.F_INTER,Config_.F_INTER).astype(float)
    Verifying_STYPES = np.array(['TS','HU','SS','TD','SD'])
    gdland = read_gdland('../Data/gdland_table.dat') 

    Int_Errors = [list() for num in range(0,len(Verifying_Hours))]
    LONS_ = [list() for num in range(0,len(Verifying_Hours))]
    LATS_  = [list() for num in range(0,len(Verifying_Hours))]



    #If this is no bias correction provided    
    if list(BC_Matrix):
        #If a Bias Correction is added, assuma a std matrix is also included
        SD_Errors  = [list() for num in range(0,len(Verifying_Hours))]
        BC_Errors  = [list() for num in range(0,len(Verifying_Hours))]


    #This loop removes duplicate entries into the atcf (including duplicate lines for different wind radii)
    Bdata_temp=[]
    Bdata2=[]
    for i,row_verif in enumerate(Bdata):
        if i==0:
            Bdata_temp.append(row_verif[2].strip())
            Bdata2.append(row_verif)
        else:
            if (row_verif[2].strip() in Bdata_temp):
#                print('Duplicate Skip')
                continue
            else: 
                Bdata_temp.append(row_verif[2].strip())
                Bdata2.append(row_verif)

    Bdata2=np.array(Bdata2)        




    for i,row_verif in enumerate(Bdata2):
        stormname = row_verif[1].strip()
        Datetime = dt.datetime.strptime(row_verif[2].strip(),'%Y%m%d%H')
        LON  = float(row_verif[7].strip()[:-1])/10.
        LAT = float(row_verif[6].strip()[:-1])/10.
        INT = float(row_verif[8].strip())
        STYPE = (row_verif[10].strip())
        
        #If the initial storm type is not verifiable
        if (~np.any(Verifying_STYPES==STYPE))|(INT<Config_.Min_Int):
            continue
            
        
        #Now if we have a potentially verifyable case
        for l2s_i,flen in enumerate(Verifying_Hours):
            VSTYPE = np.nan
            VLON = np.nan
            VLAT = np.nan
            VINT = np.nan
            VDTL = np.nan
            
            VER_DT = Datetime + dt.timedelta(hours=int(flen))
            
            #Check if there are potential verifying times
            Pindexs = np.argwhere(np.char.strip(Bdata2[:,2])== VER_DT.strftime('%Y%m%d%H'))
                                  
            if len(Pindexs)==0:
                continue
                #No Verifying forecast
                                  
            elif len(Pindexs)==1:
                #If 1 solution check if it is for the correct storm
                if (np.char.strip(Bdata2[Pindexs[0][0],1])==stormname):
                    VSTYPE = (Bdata2[Pindexs[0][0],10].strip())
                    VLON  = -float(Bdata2[Pindexs[0][0],7].strip()[:-1])/10.
                    VLAT = float(Bdata2[Pindexs[0][0],6].strip()[:-1])/10.
                    VINT = float(Bdata2[Pindexs[0][0],8].strip())
                    VDTL = check_if_land([np.nan],[VLON+360.],[VLAT],gdland)
                    
                else:
                    continue
                    #No verifying forecasts for this time and storm
                                  
            elif len(Pindexs)>1:
                #Check to see which time is a potential match
                            
                for indx in Pindexs:
                     if (np.char.strip(Bdata2[indx[0],1])==stormname):
                         VSTYPE = (Bdata2[indx[0],10].strip())
                         VLON  = -float(Bdata2[indx[0],7].strip()[:-1])/10.
                         VLAT = float(Bdata2[indx[0],6].strip()[:-1])/10.
                         VINT = float(Bdata2[indx[0],8].strip())
                         VDTL = check_if_land([np.nan],[VLON+360.],[VLAT],gdland)

                        
                        
                     else:
                         pass

            #Now check the distance to land
            if int(VDTL)<0:
                continue
                        
            if (np.any(Verifying_STYPES==VSTYPE)):
                #If we get to this block then we have a verifiable forecast!
                
                #Now we should go through the A decks and get a forecast
                for row_forc in Adata[:]:
                    fstormname = row_forc[1].strip()
                    fDatetime = dt.datetime.strptime(row_forc[2].strip(),'%Y%m%d%H')
                    fModel = row_forc[4].strip()
                    fhour = row_forc[5].strip()
                    fverif = fDatetime + dt.timedelta(hours=int(fhour))
                    fLON  = -float(row_forc[7].strip()[:-1])/10.
                    fLAT = float(row_forc[6].strip()[:-1])/10.
                    fDTL = check_if_land([fhour],[fLON+360.],[fLAT],gdland)
                    fVINT = float(row_forc[8].strip())
                    fSTYPE = (row_forc[10].strip())


                    #We don't bias correct the zeroth hour       
                    if (stormname==fstormname)&(fDatetime.strftime('%Y%m%d%H')==Datetime.strftime('%Y%m%d%H'))&(int(fhour)==int(0)):
                        Int_atinit = fVINT
                    


                    if (stormname==fstormname)&(fverif.strftime('%Y%m%d%H')==VER_DT.strftime('%Y%m%d%H'))&(int(fhour)==int(flen)):
#                        print('%i h Forecast for %s Valid %s'%(flen,stormname,fverif.strftime('%Y%m%d%H')))
                        Forec_intc = fVINT-Int_atinit
                        Actual_intc= VINT-INT
         
                        #DONT INCLUDE LANDFALL FORECASTS
                        #!----------------------------------!#
                        if int(fDTL)<0: #landfall is forecast to occur
#                            print('Not including %s forecast because DTL=%i'%(fstormname,int(fDTL)))
                            continue
                        #!----------------------------------!#
        

                        Int_Errors[l2s_i].append(Forec_intc-Actual_intc)

            
                        LATS_[l2s_i].append([fLAT,VLAT])
                        LONS_[l2s_i].append([fLON,VLON])



                        #If we have a bias correction matrix, then apply it to the forecast            
                        if list(BC_Matrix):
                            New_intc,forsd = USE_BC(BC_Matrix,SD_Matrix,int(fhour),Forec_intc)
                            BC_Errors[l2s_i].append(New_intc-Actual_intc)                        
                            SD_Errors[l2s_i].append(forsd)                       
 
                        # No need to keep the loop going if we found the forecast
                        break

                
            else:
                continue
    # If there was a bias matrix, then output the errors, the errors from the bias correction, the standard deviations, and the lat/lon                
    if list(BC_Matrix):
        return Int_Errors,BC_Errors,SD_Errors,LATS_,LONS_
    # If no bias correction matrix, then return errors and lat/lon
    else:
        return Int_Errors,LATS_,LONS_
                
            

def USE_BC(BC_matrix,SD_matrix,FHOUR,VCHANGE):
    F_name = np.arange(Config_.F_START,Config_.F_END+Config_.F_INTER,Config_.F_INTER).astype(float)

    INDEX = int(np.argwhere(F_name==FHOUR))
    
    vmins = np.array([-127.5, -117.5, -107.5, -97.5, -87.5,-77.5, -67.5,-57.5,  -47.5,\
     -37.5, -27.5, -17.5,   -7.5,-2.5,2.5, 7.5,   17.5,   27.5,  37.5, 47.5,   57.5, 67.5, 77.5,\
      87.5,  97.5,  107.5, 117.5])

    vmaxs = np.array([-117.5, -107.5, -97.5, -87.5,-77.5, -67.5,-57.5,  -47.5,\
     -37.5, -27.5, -17.5,   -7.5,-2.5,2.5, 7.5,   17.5,   27.5,  37.5, 47.5,   57.5, 67.5, 77.5,\
      87.5,  97.5,  107.5, 117.5, 127.5])
    
    
    j=0       
    for vmin,vmax in zip(vmins,vmaxs):
        
        if (j==0)&(VCHANGE<vmin):
            #If the change is less than the vmin bounds make it the edge
            BC_VCHANGE = VCHANGE-(BC_matrix[INDEX,j])
            SD_VCHANGE = SD_matrix[INDEX,j]
            return BC_VCHANGE,SD_VCHANGE    
        
        if (j==len(vmins))&(VCHANGE>vmax):
            #If the change is less than the vmin bounds make it the edge
            BC_VCHANGE = VCHANGE-(BC_matrix[INDEX,j])
            SD_VCHANGE = SD_matrix[INDEX,j]
            return BC_VCHANGE,SD_VCHANGE    
        
           
        if (VCHANGE<vmax)&(VCHANGE>vmin):
            BC_VCHANGE = VCHANGE-(BC_matrix[INDEX,j])
            SD_VCHANGE = SD_matrix[INDEX,j]
            return BC_VCHANGE,SD_VCHANGE
        


        j+=1

    
    raise Exception("Intensity Change not in the Bias Correction")



def datafgrep(bashcommand):
    
    try:
        output = subprocess.check_output(bashcommand, shell=True)
        output = str(output).split('\\n')
    except:
        return []
    
    data = []
    for i,val in enumerate(output):
        #A b' shows up when converting from bytes to string
        if i==0:
            data.append(val[2:].split(','))
            
        elif len(val)>=10:
            data.append(val.split(','))
            
    return np.array(data)


def bestgrep(bashcommand):

    output = subprocess.check_output(bashcommand, shell=True)
    
    output = str(output).split('\\n')
    
    data = []
    for i,val in enumerate(output):
        #A b' shows up when converting from bytes to string
        if (i==0):
            length = len(val.split(','))
            ntoad = 44-int(length)
            new_val = val+ntoad*', '
            data.append(new_val[2:].split(','))

        elif len(val.split(','))>4:
            length = len(val.split(','))
            ntoad = 44-int(length)
            new_val = val+ntoad*', '
            data.append(new_val.split(','))


    return np.array(data)



def get_model_deck(MODEL,STORMPATH):
    bash_arg = 'grep '+MODEL+ ' '+ STORMPATH
    return datafgrep(bash_arg)


def get_model_bdeck(MODEL,STORMPATH):
    bash_arg = 'grep '+MODEL+ ' '+ STORMPATH
    return bestgrep(bash_arg)



def get_Year_aids(YYYY,MODEL,FILEPATH,BASIN):
    import glob
    
    #Get a list of all the files
    filelist= sorted(glob.glob(FILEPATH+'a'+BASIN.lower()+'[0-4][0-9]'+YYYY+'.dat')) 
    
    #Create the array by using the first case and then adding to it
    ALLDat = get_model_deck(MODEL,filelist[0])
    print('Getting %s Data for %s'%(MODEL,filelist[0].split('/')[-1]))
    
    for filename in filelist[1:]: #Loop through subsequent arrays
        print('Getting %s Data for %s'%(MODEL,filename.split('/')[-1]))
        ALLDat = np.vstack((ALLDat,get_model_deck(MODEL,filename)))
        
    return ALLDat
        
def get_Year_best(YYYY,FILEPATH,BASIN):
    import glob
    
    #Get a list of all the files
    filelist= sorted(glob.glob(FILEPATH+'b'+BASIN.lower()+'[0-4][0-9]'+YYYY+'.dat')) 
    
    #Create the array by using the first case and then adding to it
    ALLDat = get_model_bdeck('BEST',filelist[0])
    print('Getting %s Data for %s'%('BEST',filelist[0].split('/')[-1]))
    
    for filename in filelist[1:]: #Loop through subsequent arrays
        print('Getting %s Data for %s'%('BEST',filename.split('/')[-1]))
        ALLDat = np.vstack((ALLDat,get_model_bdeck('BEST',filename)))
        
    return ALLDat
        

def get_Year_verif(YYYYS,FILEPATH,BASIN,MODEL):
    ##################################
    
    ALLDat = []
    ALLDatN = []
  
    for YYYY in YYYYS:
        #Get a list of all the files in adecks and bdecks
        Bfilelist= sorted(glob.glob(FILEPATH+'b'+BASIN+'[0-4][0-9]'+YYYY+'.dat'))
        Afilelist= sorted(glob.glob(FILEPATH+'a'+BASIN+'[0-4][0-9]'+YYYY+'.dat'))

        for filenameA,filenameB in zip(Afilelist,Bfilelist):
            if (filenameB.split('/')[-1][1:])!=(filenameA.split('/')[-1][1:]):
                continue
            
            print('Verifying %s'%(filenameB.split('/')[-1][1:]))
            #For each storm
            Adata = get_model_deck(MODEL,filenameA)        
            Bdata = get_model_bdeck('BEST',filenameB)
        
            Vdata = verif_int(MODEL,Adata,Bdata)
            Samps = np.array([len(var) for var in Vdata])
            Error = np.array([np.nanmean(np.abs(var)) for var in Vdata])
            ALLDat.append(Error)
            ALLDatN.append(Samps)

        


    return np.array(ALLDatN), np.array(ALLDat)


def remove_multi_adeck(adeck):
    # This function removes multiple forecast rows from multiple wind radii
    if len(adeck)==0:
        return []

    fh_list = []
    fd_list = []

    ALLDATA=[]
    for i,line in enumerate(adeck):
        fDatetime = dt.datetime.strptime(line[2].strip(),'%Y%m%d%H')
        fhour = int(line[5].strip())

        if i==0:
            fh_list.append(fhour)
            fd_list.append(fDatetime)
            ALLDATA.append(line)

        else:
            zz=0
            for fd,fh in zip(fd_list,fh_list):
                #If the forecast length and initialization time is the same-then skip
                if ((fh==fhour) & (fd==fDatetime)):
                    zz=999
                    break

            if zz==999:
                continue
            elif zz==0:
                ALLDATA.append(line)
                fh_list.append(fhour)
                fd_list.append(fDatetime)

    return ALLDATA


def get_Year_verif_BC(YYYYS,BASIN,MODEL,FILEPATHA,FILEPATHB,FILEPATHI):
    ##################################
    
    ALLDat = []
    ALLDatN = []
    ALLDatBC = []
    ALLDatSD = []

    
    ##################################
#    BC_matrix,SD_matrix = get_BC_(MODEL,BASIN)

    MODELNAME, BASIN, BSCHEME, XBIN, YBINL, YBINU = read_IBUS(FILEPATHI+'%s_%s_Bias.txt'%(MODEL,BASIN))
    MODELNAME, BASIN, USCHEME, XBIN, YBINL, YBINU = read_IBUS(FILEPATHI+'%s_%s_Uncr.txt'%(MODEL,BASIN))


    Verifying_Hours = np.arange(Config_.F_START,Config_.F_END+Config_.F_INTER,Config_.F_INTER).astype(float)

    Int_Errors = [list() for num in range(0,len(Verifying_Hours))]
    LONS_ = [list() for num in range(0,len(Verifying_Hours))]
    LATS_  = [list() for num in range(0,len(Verifying_Hours))]
   
    SD_Errors  = [list() for num in range(0,len(Verifying_Hours))]
    BC_Errors  = [list() for num in range(0,len(Verifying_Hours))]

    
    ##################################
    STORM_NAMES= []

    for YYYY in YYYYS:
        ##################################
        #Get a list of all the files in bdecks
        Bfilelist= sorted(glob.glob(FILEPATHB+'b'+BASIN.lower()+'[0-4][0-9]'+YYYY+'.dat'))

        ##################################
        #Get a list of all the files in adecks
        Afilelist= sorted(glob.glob(FILEPATHA+'a'+BASIN.lower()+'[0-4][0-9]'+YYYY+'.dat'))


        for filenameA,filenameB in zip(Afilelist,Bfilelist):
            
            if (filenameB.split('/')[-1][1:])!=(filenameA.split('/')[-1][1:]):
                continue
                
            #For each storm
            Adata = remove_multi_adeck(get_model_deck(MODEL,filenameA))
            Bdata = get_model_bdeck('BEST',filenameB)
        
            if len(Adata)==0:
                continue

            print('Verifying %s'%(filenameB.split('/')[-1][1:]))
            STORM_NAMES.append(filenameB.split('/')[-1][1:][:-4])
        
            Vdata, BCdata, SDdata,LAT_d,LON_d = verif_ic_wbc(MODEL,Adata,Bdata,BSCHEME.T,USCHEME.T)
            Samps = np.array([len(var) for var in Vdata])
            IC = np.array([np.nanmean(np.abs(var)) for var in Vdata])
            BC = np.array([np.nanmean(np.abs(var)) for var in BCdata])
            SD = []

            SD = np.array([np.corrcoef(np.abs(var[0]),np.abs(var[1]))[1,0] for var in zip(SDdata, Vdata)])
             
            for ltind in np.arange(0,len(Vdata)):
                [SD_Errors[ltind].append(element) for element in SDdata[ltind]]
#             [Int_Errors[ltind].append(np.abs(element[0])-np.abs(element[1])) for element in zip(Vdata[ltind],BCdata[ltind])]
                [Int_Errors[ltind].append(element) for element in np.abs(Vdata[ltind])]
                [LONS_[ltind].append(element) for element in LON_d[ltind]]
                [LATS_[ltind].append(element) for element in LAT_d[ltind]]



        
            ALLDat.append(IC)
            ALLDatN.append(Samps)
            ALLDatBC.append(BC)
            ALLDatSD.append(SD)


       

    return np.array(ALLDatN), np.array(ALLDat), np.array(ALLDatBC), np.array(ALLDatSD),SD_Errors,Int_Errors,STORM_NAMES,LATS_,LONS_




def get_cor_p(VAR1,VAR2):
    F_name = np.arange(Config_.F_START,Config_.F_END+Config_.F_INTER,Config_.F_INTER).astype(float)

    SD = []
    Pval=[]
    for fv in range(len(F_name)):
        try:
            mask_ = ~np.isnan(VAR1[fv]) & ~np.isnan(VAR2[fv])
            var0 = np.abs(np.array(VAR1[fv]))
            var1 = np.abs(np.array(VAR2[fv]))
            
            if int(len(var1))==0:
                SD.append(np.nan)
                Pval.append(np.nan)

            else:
                sy, iy, r_valy, p_vy, std_err = stats.linregress(var0[mask_],var1[mask_])
                SD.append(r_valy)
                Pval.append(p_vy)
        except:
            continue


    return np.array(SD), np.array(Pval)


def plot_corr(SD_data,SER_data,SNdata, BASIN, MODEL, YYYY,Path_to_output):
    F_name = np.arange(Config_.F_START,Config_.F_END+Config_.F_INTER,Config_.F_INTER).astype(float)


    fig = plt.figure(figsize=(7,5))
    ax1 = plt.subplot(111)
    #ax2 = ax1.twinx()
    #ax2 = plt.subplot(212)


    axes = [ax1]





    var_exp = (np.array(100.*get_cor_p(SD_data,SER_data)[0]**2).astype(float))
    print(var_exp)


    ax1.plot(F_name,(get_cor_p(SD_data,SER_data)[0]),lw=3,c='C0')

    #ax2.plot(F_name,var_exp,lw=3,c='r')


    ax1.scatter(F_name[get_cor_p(SD_data,SER_data)[1]>=.05],(get_cor_p(SD_data,SER_data)[0])[get_cor_p(SD_data,SER_data)[1]>=.05],c='C0',s=150,marker='o',edgecolor='k',zorder=100)

    ax1.scatter(F_name[get_cor_p(SD_data,SER_data)[1]<.05],(get_cor_p(SD_data,SER_data)[0])[get_cor_p(SD_data,SER_data)[1]<.05],c='C0',s=200,marker='*',edgecolor='k',zorder=100)


    cutedge(ax1)

    #ax1.legend(loc=1,prop={'size':14})
    ax1.set_xticks(F_name)

    ax1.set_ylabel('Error-STDE Correlation',fontsize=14)
    ax1.set_xlabel('Forecast Hour ',fontsize=14)
    ax1.set_ylim(-1.0,1.0)
    #ax2.set_ylim(0,100)
    ax1.axhline(0,c='k',ls='dotted',lw=2,zorder=-1)


    props1=dict(facecolor='None',alpha=.9,edgecolor='None')

    for i,xx in enumerate(F_name):
        ax1.text(xx,-.88,np.sum(SNdata,axis=0)[i],fontsize=12,color='b', bbox=props1,ha="center",va='center')

        props2=dict(facecolor='white',alpha=.9,edgecolor='none')
        ax1.text(.03,1.02,BASIN,fontsize=16,color='k', bbox=props2,ha="left",va='bottom',transform=ax1.transAxes)
        ax1.text(.97,1.02,'-'.join(YYYY),fontsize=16,color='k', bbox=props2,ha="right",va='bottom',transform=ax1.transAxes)
        ax1.text(.5,1.02,MODEL,fontsize=16,color='k', bbox=props2,ha="center",va='bottom',transform=ax1.transAxes)

    if Path_to_output:
        plt.savefig(Path_to_output+'/'+'%s_%s_%s_error_std_cor.pdf'%(MODEL,BASIN,'_'.join(YYYY)),bbox_inches='tight')
        plt.close(fig)

    else:
        plt.show()



def plot_maeskill(Sdata,SBdata,SNdata, BASIN, MODEL, YYYY,Path_to_output):
    #Makes a 2 panel plot of the mae of each storm and mean and model skill for each storm and mean

    #Get the forecast hours
    F_name = np.arange(Config_.F_START,Config_.F_END+Config_.F_INTER,Config_.F_INTER).astype(float)

    #Define the plot
    fig = plt.figure(figsize=(16,5))
    ax = plt.subplot(121)
    ax2 = plt.subplot(122)


    #Plot each of the storms errors as a gray line
    ax.plot(F_name,Sdata.T,c='gray',marker='o',lw=1.5,alpha=.3,markersize=5)
    #Plot the mean
    ax.plot(F_name,np.nansum(Sdata*SNdata,axis=0)/np.sum(SNdata,axis=0),lw=3,c='k',markersize=10,marker='o',label='Weighted Mean')

    #plot the bias corrected storm errors
    ax.plot(F_name,SBdata.T,c='orange',marker='o',lw=1.5,alpha=.3,markersize=5)
    ax.plot(F_name,np.nansum(SBdata*SNdata,axis=0)/np.sum(SNdata,axis=0),lw=3,c='C3',markersize=10,marker='o',label='Bias Corrected Weighted Mean')


    #Plot the skill for each storm
    ax2.plot(F_name,skill(Sdata.T,SBdata.T),c='C0',marker='o',lw=1.5,alpha=.3,markersize=5)
    #Plot the skill between the bias corrected mean and unbiased mean
    ax2.plot(F_name,skill(np.nansum(Sdata*SNdata,axis=0)/np.sum(SNdata,axis=0),np.nansum(SBdata*SNdata,axis=0)/np.sum(SNdata,axis=0)),lw=3,c='k',markersize=10,marker='o',label='Corrected Weighted Mean')


    #indicate the zero skill line
    ax2.axhline(0,c='gray',lw=2,ls='dotted')

    ax2.set_ylabel('Skill (%)',fontsize=14)
    ax2.set_xlabel('Forecast Hour ',fontsize=14)
    ax2.set_ylim(-40,40)


    #custom function to increase tick number sizes and cut off the top and right edge of frame
    cutedge(ax)
    cutedge(ax2)
    
    #add the legends
    ax.legend(loc=1)
    ax2.legend(loc=1)

    #make sure the ticks are 12 hourly 
    ax.set_xticks(F_name)
    ax2.set_xticks(F_name)
    
    #set labels and format the axis
    ax.set_ylabel('MAE (kt)',fontsize=14)
    ax.set_xlabel('Forecast Hour ',fontsize=14)
    ax.set_ylim(-2,45)
    ax.set_yticks(np.arange(0,50,5))


    #set properties of the sample size text
    props1=dict(facecolor='None',alpha=.9,edgecolor='None')

    for i,xx in enumerate(F_name): #loop through forecast hours and add sample sizes to plot
        ax.text(xx,-.5,np.sum(SNdata,axis=0)[i],fontsize=12,color='r', bbox=props1,ha="center",va='center')
        ax2.text(xx,-38,np.sum(SNdata,axis=0)[i],fontsize=12,color='r', bbox=props1,ha="center",va='center')


    #set properties of storm and datetime indifiers
    props2=dict(facecolor='white',alpha=.9,edgecolor='None')
    ax.text(.05,1.02,BASIN.upper(),fontsize=16,color='k', bbox=props2,ha="left",va='bottom',transform=ax.transAxes,zorder=99999)
    ax.text(.5,1.02,MODEL,fontsize=16,color='k', bbox=props2,ha="center",va='bottom',transform=ax.transAxes,zorder=99999)
    ax.text(.95,1.02,'-'.join(YYYY),fontsize=16,color='k', bbox=props2,ha="right",va='bottom',transform=ax.transAxes,zorder=99999)

    ax2.text(.05,1.02,BASIN.upper(),fontsize=16,color='k', bbox=props2,ha="left",va='bottom',transform=ax2.transAxes,zorder=99999)
    ax2.text(.5,1.02,MODEL,fontsize=16,color='k', bbox=props2,ha="center",va='bottom',transform=ax2.transAxes,zorder=99999)
    ax2.text(.95,1.02,'-'.join(YYYY),fontsize=16,color='k', bbox=props2,ha="right",va='bottom',transform=ax2.transAxes,zorder=99999)


    #If we have a path to save the data use it, otherwise show the figure
    if Path_to_output:
        plt.savefig(Path_to_output+'/'+'%s_%s_%s_MAE_Skill.pdf'%(MODEL,BASIN,'_'.join(YYYY)),bbox_inches='tight')
        plt.close(fig)

    else:
        plt.show()




def sbs_MAE(Sdata,SBdata,STORM_NAMES, BASIN, MODEL, YYYY,Path_to_output):
    #This plot is a 'Storm by storm' (SBS) plot showing the mean impact on each storm
    #Provides an overview of what storms were helped or hurt by the bias correction

    # The forecast hours (Note that F_name2 does not include zero)
    F_name2 = np.arange(Config_.F_START,Config_.F_END+Config_.F_INTER,Config_.F_INTER).astype(float)
    F_name = np.arange(0,Config_.F_END+Config_.F_INTER,Config_.F_INTER).astype(float)

    #Get the storm names
    STORMNAMES = [val[:4] for val in STORM_NAMES]


    #define the figure
    fig = plt.figure(figsize=(10,10))
    ax1 = plt.subplot(211)
    ax2 = plt.subplot(212)


    axes = [ax1,ax2] #make a list of subplots to loop through later

    x,y = np.meshgrid(np.arange(0,len(STORM_NAMES)+1),F_name)

    current_cmap = plt.cm.Reds
    current_cmap.set_bad(color='gray')

    # Plot the sample size of forecasts
    im = ax1.pcolormesh(x,y,(SBdata).T,cmap=current_cmap,vmin=0,vmax=30,edgecolors='k',linewidths=.1)

    #Set the colormap
    current_cmap = plt.cm.seismic
    current_cmap.set_bad(color='gray')

    # Plot the difference in the errors
    im2 = ax2.pcolormesh(x,y,(Sdata-SBdata).T,cmap=current_cmap,vmin=-10,vmax=10,edgecolors='k',linewidths=.1)


    # Format the axes with labels
    for ax in axes:
        cutedge(ax)
        ax.set_yticks(F_name[:-1]+6)
        ax.set_yticklabels(F_name2.astype(np.int))
        ax.set_xticks(np.arange(0,len(STORM_NAMES))+.5)
        ax.set_xticklabels(STORMNAMES,rotation=90)
        ax.set_ylabel('Forecast Hour ',fontsize=14)


    props1=dict(facecolor='None',alpha=.9,edgecolor='None')

    # Set the colorbars
    cb = plt.colorbar(im,pad=.01, shrink=.8,ax=ax1)
    cb.set_label('MAE (kt)',fontsize=14)

    cb2 = plt.colorbar(im2,pad=.01, shrink=.8,ax=ax2)
    cb2.set_label('MAE Difference (kt)',fontsize=14)


    props2=dict(facecolor='white',alpha=.9,edgecolor='None')
    ax1.text(.05,1.02,BASIN.upper(),fontsize=16,color='k', bbox=props2,ha="left",va='bottom',transform=ax1.transAxes,zorder=99999)
    ax1.text(.5,1.02,MODEL,fontsize=16,color='k', bbox=props2,ha="center",va='bottom',transform=ax1.transAxes,zorder=99999)
    ax1.text(.95,1.02,'-'.join(YYYY),fontsize=16,color='k', bbox=props2,ha="right",va='bottom',transform=ax1.transAxes,zorder=99999)

    # If a path is provided then save to that location
    if Path_to_output:
        plt.savefig(Path_to_output+'/'+'%s_%s_%s_sbs_MAEwDiff.pdf'%(MODEL,BASIN,'_'.join(YYYY)),bbox_inches='tight')
        plt.close(fig)

    # If no path is set, then just display the plot
    else:
        plt.show()

#
#
#CONSENSUS FUNCTIONS are similar to those above but slightly modified
#
#



def Get_IBUS_CONS(STORMID,DTofAn,BASIN,pathtoadeck,pathtoscheme):
    #This code will create a consensus forecast for a specific forecast time based on the availability of the 
    # forecast models in the config files
 
    pathtoa = pathtoadeck+'a'+STORMID.lower()+'.dat'

    #Read in the A deck but only for the specified time
    Adata=remove_multi_afor(parseadeck(getadeck(STORMID,pathtoadeck,DTofAn)))
       
    if len(Adata)==0: #If there is an empty a-deck then skip
        return [np.nan], [np.nan], [np.nan], [np.nan], [np.nan], [np.nan]

    else: #If there is something in the file then lets run the IBUS

        #Open the file to start adding data

        Conf_Data = Consensus_Config(BASIN)

        #Use model names specified in consensus config file
        MODELNAMES = Conf_Data[0][Conf_Data[3]=='1']

        MODELBIAS_INC = Conf_Data[0][Conf_Data[1]=='1']
        MODELUNCR_INC = Conf_Data[0][Conf_Data[2]=='1']


        #This will be the data for all the models
        CON_FHOUR   = []
        CON_FINT    = []
        CON_FLAT    = []
        CON_FLON    = []
        CON_FBIAS   = []
        CON_FSTDE   = []

        #First loop through models and add them to an output file
        ###################################
        for MODELNAME in MODELNAMES: #If there are multiple models to include the IBUS for
            #Get the data for the analysis
            try:
                Mdata = grabadeck(Adata,MODELNAME)
            except:
                continue
            #Not every model may have run, if the model was not run make that note
            if len(Mdata)==0: #if there is no data here
                #Output that No data exists
                continue

            # read in the Schemes for the model being looped over
            MODELNAME, BASIN, USCHEME, XBIN, YBINL, YBINU = read_IBUS(pathtoscheme+'%s_%s_Uncr.txt'%(MODELNAME,BASIN))    
            MODELNAME, BASIN, BSCHEME, XBIN, YBINL, YBINU = read_IBUS(pathtoscheme+'%s_%s_Bias.txt'%(MODELNAME,BASIN))  

            # Data at initial time
            O_IDTM = Mdata[0,0].strftime('%Y%m%d%H')
            O_IINT = Mdata[4,0]

            #Declare arrays with Forecast data
            O_FHOUR = XBIN
            O_FINT = np.empty(len(XBIN))
            O_FLON = np.empty(len(XBIN))
            O_FLAT = np.empty(len(XBIN))
            O_FBIAS = np.empty(len(XBIN))
            O_FSTDE = np.empty(len(XBIN))

            #Fill the arrays with 999 for missing values
            O_FINT[:] = np.nan
            O_FLON[:] = np.nan
            O_FLAT[:] = np.nan
            O_FBIAS[:] = np.nan
            O_FSTDE[:] = np.nan

            ##########################
            ii=0 #index of output array
            i_dat = 0 #index of data array
            for FHOUR,FVCHANGE in zip(Mdata[1,:],Mdata[6,:]):
                #note that forecast hours were already sorted
                if str(FHOUR)=='0':
                    # Data at initial time
                    O_IDTM = Mdata[0,i_dat].strftime('%Y%m%d%H')
                    O_IINT = Mdata[4,i_dat]

                elif str(FHOUR) in XBIN:
                    #If the forecast hour is one that is used in the scheme then apply IBUS
                    BiasFC,STDEFC = BC_Values(BSCHEME,USCHEME,FHOUR,FVCHANGE,XBIN,YBINL,YBINU)
                    O_FHOUR[ii]= int(FHOUR)
                    O_FINT[ii]= int(Mdata[4,i_dat])
                    O_FLAT[ii]= float(Mdata[2,i_dat])
                    O_FLON[ii]= float(Mdata[3,i_dat])


                    if MODELNAME in MODELBIAS_INC:
                        O_FBIAS[ii]= np.float(BiasFC)
                    else:
                        O_FBIAS[ii]=0

                    if MODELNAME in MODELUNCR_INC:
                        O_FSTDE[ii]= np.float(STDEFC)
                    else:
                        O_FSTDE[ii]= np.nan


                    ii+=1

                i_dat+=1
                
            #Now save the arrays to CON_lists
            CON_FHOUR.append(O_FHOUR)
            CON_FINT.append(O_FINT)
            CON_FLAT.append(O_FLAT)
            CON_FLON.append(O_FLON)
            CON_FBIAS.append(O_FBIAS)
            CON_FSTDE.append(O_FSTDE)



        #Now we have all the data for the consensus models
        CON_FINT = np.squeeze(CON_FINT)
        CON_FLAT = np.squeeze(CON_FLAT)
        CON_FLON = np.squeeze(CON_FLON)
        CON_FHOUR = np.squeeze(CON_FHOUR)
        CON_FBIAS = np.squeeze(CON_FBIAS)
        CON_FSTDE = np.squeeze(CON_FSTDE)

        #Create new arrays for theconsensus forecast
        CONSENSUS_FORECAST      = np.zeros(CON_FHOUR.shape[1])
        CONSENSUS_DB_FORECAST   = np.zeros(CON_FHOUR.shape[1])
        CONSENSUS_DBV_FORECAST  = np.zeros(CON_FHOUR.shape[1])
        CONSENSUS_UNC_FORECAST  = np.zeros(CON_FHOUR.shape[1])
        CONSENSUS_LAT_FORECAST  = np.zeros(CON_FHOUR.shape[1])
        CONSENSUS_LON_FORECAST  = np.zeros(CON_FHOUR.shape[1])

        #put in nans for missing values
        CONSENSUS_FORECAST[:]      = np.nan
        CONSENSUS_DB_FORECAST[:]   = np.nan
        CONSENSUS_DBV_FORECAST[:]  = np.nan
        CONSENSUS_UNC_FORECAST[:]  = np.nan
        CONSENSUS_LAT_FORECAST[:]  = np.nan
        CONSENSUS_LON_FORECAST[:]  = np.nan


        # now loop through the forecast hours
        for ind in range(0,CON_FHOUR.shape[1],1):
            #Make sure there are atleast 2 non-nan forecasts for each forecast hour
            if np.count_nonzero(~np.isnan(CON_FINT[:,ind]))>=Config_.CON_MIN:
                CONSENSUS_LON_FORECAST[ind] = np.nanmean(CON_FLON[:,ind],axis=0) #Mean of forecasts
                CONSENSUS_LAT_FORECAST[ind] = np.nanmean(CON_FLAT[:,ind],axis=0) #Mean of forecasts

                CONSENSUS_FORECAST[ind] = np.nanmean(CON_FINT[:,ind],axis=0) #Mean of forecasts
                CONSENSUS_DB_FORECAST[ind] = np.nanmean((CON_FINT[:,ind]-CON_FBIAS[:,ind]),axis=0)#Mean of simple debias (not used right now)
                #Mean weighted by the uncertainty from intensity change scheme
                CONSENSUS_DBV_FORECAST[ind] = np.nanmean((CON_FINT[:,ind]-(CON_FSTDE[:,ind]*CON_FBIAS[:,ind])/np.nansum(CON_FSTDE[:,ind],axis=0)),axis=0)
                #Take the mean uncertainty
#                WEIGHTS = (CON_FSTDE[:,ind].astype(float)/np.nansum(CON_FSTDE[:,ind],axis=0))
#                INVERTED_WEIGHTS = ((1.-WEIGHTS))/(np.nansum((1.-WEIGHTS)))
#                CONSENSUS_DBV_FORECAST[ind] = np.nansum(INVERTED_WEIGHTS*(CON_FINT[:,ind]-CON_FBIAS[:,ind]),axis=0)


                CONSENSUS_UNC_FORECAST[ind] = np.nanmean(CON_FSTDE[:,ind],axis=0)


    return XBIN, CONSENSUS_FORECAST, CONSENSUS_DBV_FORECAST, CONSENSUS_UNC_FORECAST,CONSENSUS_LON_FORECAST,CONSENSUS_LAT_FORECAST
            


def Verif_Consensus(STORMID,BASIN,pathtoadeck,pathtobdeck,pathtoscheme,gdland):
    # Given a specific stormid, this code will calculate the intensity errors and errors with the bias correction
    # Only used for a consensus forecast

    #Get the locatio of the a- and b- deck files
    adeckfile = pathtoadeck+'a'+STORMID.lower()+'.dat'
    bdeckfile = pathtobdeck+'b'+STORMID.lower()+'.dat'

    Bdata = get_model_bdeck('BEST',bdeckfile) #grab the bdeck data
    
    Conf_Data = Consensus_Config(BASIN) #Get the consensus storms from teh confif file

    MODELNAMES = Conf_Data[0][Conf_Data[3]=='1'] #get all teh model names to be used in this consensus

    MODELBIAS_INC = Conf_Data[0][Conf_Data[1]=='1'] #get models for the bias correction
    MODELUNCR_INC = Conf_Data[0][Conf_Data[2]=='1'] #get the models for the uncertainty scheme

    #Get the forecast hour list from the config file
    Verifying_Hours = np.arange(Config_.F_START,Config_.F_END+Config_.F_INTER,Config_.F_INTER).astype(float)

    #only these verifying storm times are included in the verification
    Verifying_STYPES = np.array(['TS','HU','SS','TD','SD'])

    #Define emtpy list of lists for each verifying hour
    Int_Errors = [list() for num in range(0,len(Verifying_Hours))]
    SD_Errors  = [list() for num in range(0,len(Verifying_Hours))]
    BC_Errors  = [list() for num in range(0,len(Verifying_Hours))]


    #This is a check to remove any duplicate rows of best track data
    # Keeps only one row for the different wind speed radii threshholds
    Bdata_temp=[]
    Bdata2=[]
    for i,row_verif in enumerate(Bdata):
        if i==0:
            Bdata_temp.append(row_verif[2].strip())
            Bdata2.append(row_verif)
        else:
            if (row_verif[2].strip() in Bdata_temp):
#                print('Duplicate Skip')
                continue
            else: 
                Bdata_temp.append(row_verif[2].strip())
                Bdata2.append(row_verif)

    Bdata2=np.array(Bdata2)        

    #Now loop through the best track data
    for i,row_verif in enumerate(Bdata2):
        Datetime = dt.datetime.strptime(row_verif[2].strip(),'%Y%m%d%H')
        INT = float(row_verif[8].strip())
        STYPE = (row_verif[10].strip())

        #If the initial storm type is not verifiable
        if (~np.any(Verifying_STYPES==STYPE))|(INT<Config_.Min_Int):
            continue


        #Now if we have a potentially verifyable case
        for l2s_i,flen in enumerate(Verifying_Hours):
            VSTYPE = np.nan
            VINT = np.nan

            VER_DT = Datetime + dt.timedelta(hours=int(flen))

            #Check if there are potential verifying times
            Pindexs = np.argwhere(np.char.strip(Bdata2[:,2])== VER_DT.strftime('%Y%m%d%H'))

            if len(Pindexs)==1:
                #If 1 solution check if it is for the correct storm
                VSTYPE = (Bdata2[Pindexs[0][0],10].strip())
                VLON  = -float(Bdata2[Pindexs[0][0],7].strip()[:-1])/10.
                VLAT = float(Bdata2[Pindexs[0][0],6].strip()[:-1])/10.
                VINT = float(Bdata2[Pindexs[0][0],8].strip())
                VDTL = check_if_land([np.nan],[VLON+360.],[VLAT],gdland)


            else:
                continue #no verifying forecasts


            #Now check the distance to land
            if int(VDTL)<0:
                continue


            if (np.any(Verifying_STYPES==VSTYPE)):
                #If we get to this block then we have a verifiable forecast!
                
                #Now use the date to get the consensus forecast that is available
                fh_, CONS_FORECAST, CONS_DB_FORECAST,CONS_UNCR,CONS_LON,CONS_LAT  = Get_IBUS_CONS(STORMID,Datetime.strftime('%Y%m%d%H'),BASIN,pathtoadeck,pathtoscheme)
                
                if np.isnan(CONS_FORECAST[0]):
                    continue 
                #Now we should go through the A decks and get a forecast
                for inic_ in range(0,len(fh_)):
                    
                    fDatetime = Datetime
                    fhour   = fh_[inic_]
                    fverif  = fDatetime + dt.timedelta(hours=int(fhour))
                    fVINT   = float(CONS_FORECAST[inic_])
                    fVINTDB = float(CONS_DB_FORECAST[inic_])
                    forsd   = float(CONS_UNCR[inic_])
                    fDTL    = check_if_land([fhour],[float(CONS_LON[inic_])+360.],[float(CONS_LAT[inic_])],gdland)

                    if fDTL<0:
                        continue

                    #If the verification time matches, then subtract off actual intensity value
                    if (fverif.strftime('%Y%m%d%H')==VER_DT.strftime('%Y%m%d%H'))&(int(fhour)==int(flen)):
                        Int_Errors[l2s_i].append(fVINT-VINT)
                        BC_Errors[l2s_i].append(fVINTDB-VINT)                        
                        SD_Errors[l2s_i].append(forsd)                        

                        break #There can only be one verifiable forecast per forecast hour so break if found it


    #return the errors, the bias corrected errors, and the standard deviation of errors provided by IBUS            
    return Int_Errors,BC_Errors,SD_Errors


def CONSENSUS_Year_Verif_BC(YYYYS,BASIN,FILEPATHA,FILEPATHB,FILEPATHI):
    # Main script called that performs the verification of the consensus forecasts
    #
    ##################################

    #Read in the distance to land file
    gdland = read_gdland('../Data/gdland_table.dat')
    
    Verifying_Hours = np.arange(Config_.F_START,Config_.F_END+Config_.F_INTER,Config_.F_INTER).astype(float)


    ALLDat = []
    ALLDatN = []
    ALLDatBC = []
    ALLDatSD = []

    
    ##################################
#    BC_matrix,SD_matrix = get_BC_(MODEL,BASIN)
    Int_Errors = [list() for num in range(0,len(Verifying_Hours))]
    SD_Errors  = [list() for num in range(0,len(Verifying_Hours))]
    BC_Errors  = [list() for num in range(0,len(Verifying_Hours))]

    
    ##################################
    STORM_NAMES= []

    for YYYY in YYYYS:
        ##################################
        #Get a list of all the files in a and bbdecks within the specified year
        Bfilelist= sorted(glob.glob(FILEPATHB+'b'+BASIN.lower()+'[0-4][0-9]'+YYYY+'.dat'))
        Afilelist= sorted(glob.glob(FILEPATHA+'a'+BASIN.lower()+'[0-4][0-9]'+YYYY+'.dat'))



        for filenameA,filenameB in zip(Afilelist,Bfilelist):
        
            if (filenameB.split('/')[-1][1:])!=(filenameA.split('/')[-1][1:]):
                continue
            
            STORMID = filenameB.split('/')[-1][1:][:-4]
            print('Verifying %s'%(STORMID))
        
            STORM_NAMES.append(STORMID)
        
            #Now get the statistics for the consensus performance for this storm
            Vdata, BCdata, SDdata = Verif_Consensus(STORMID,BASIN,FILEPATHA,FILEPATHB,FILEPATHI,gdland)
        
            Samps = np.array([len(var) for var in Vdata])
            IC = np.array([np.nanmean(np.abs(var)) for var in Vdata])
            BC = np.array([np.nanmean(np.abs(var)) for var in BCdata])
            SD = []

            SD = np.array([np.corrcoef(np.abs(var[0]),np.abs(var[1]))[1,0] for var in zip(SDdata, Vdata)])
 
            for ltind in np.arange(0,len(Vdata)):
                [SD_Errors[ltind].append(element) for element in SDdata[ltind]]
                [Int_Errors[ltind].append(element) for element in np.abs(Vdata[ltind])]

        
            ALLDat.append(IC)
            ALLDatN.append(Samps)
            ALLDatBC.append(BC)
            ALLDatSD.append(SD) 


    return np.array(ALLDatN), np.array(ALLDat), np.array(ALLDatBC), np.array(ALLDatSD),SD_Errors,Int_Errors,STORM_NAMES
























