#!/usr/bin/env python
import numpy as np
from matplotlib import pyplot as plt
from read_IBUS_input import read_IBUS
from IBUS_util import Config_


###############################################
# Declaritives
#

MODELNAME = 'HWFI'
BASIN ='AL'
path_to_save=False #Set to False to show, or use '../Output' to save to output directory

MAKE_ALL=False #If set to True, it overwrites above options and uses Namelist Models and saves to '../Output'

###############################################
# Function Declarations

def cutedge(ax):
    ax.tick_params(axis='y',labelsize=12)
    ax.tick_params(axis='x',labelsize=12)
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    

    
def moddict():
    modelc = np.genfromtxt('../Config/model_cparams.txt',dtype=str,delimiter=',',skip_header=1)
    markers=['*','D', '+', 'v',4,5,6,7,8,9, '.', 'o', ' ','x','^', 's', 'p', 'h', ',',]


    mcdict = {}
    mftdict = {}
    mmdict = {}
    for line in modelc[:]:
        mcdict[line[0]]=line[1].strip()
        mmdict[line[0]]=markers[int(line[3].strip())]
        mftdict[line[0]]=int(line[4].strip())

    return mcdict,mmdict,mftdict
    
def plot_BC_UC(MODELNAME,BASIN,path_to_save=False):
    
    #First read in the bias and uncertainty files
    MODELNAME, BASIN, USCHEME, XBIN, YBINL, YBINU = read_IBUS('../Input/%s_%s_Uncr.txt'%(MODELNAME,BASIN))
    MODELNAME, BASIN, BSCHEME, XBIN, YBINL, YBINU = read_IBUS('../Input/%s_%s_Bias.txt'%(MODELNAME,BASIN))
    
    #get the model data, only really used for end times of model data resolution
    mcdict,mmdict,mftdict = moddict()

    #Define the boundaries of the plot
    ylims = [-88,88]
    xlims = [0,mftdict[MODELNAME]] #the max forecast time is set in Config/model_cparams.txt

    XBIN2 = np.insert(XBIN,0,'0') #zero is added to make the bins centered on forecast hour
    FBS = XBIN2[:-1].astype(float) + ((XBIN2[1:].astype(float)-XBIN2[:-1].astype(float))/2.) #get the centered bin

    YBIN2 = np.insert(YBINL,-1,YBINU[-1]) #now add the top value to center the y-axes bins
    VMS = YBIN2[:-1]+((YBIN2[1:]-YBIN2[:-1])/2.) #centered ybin points

    #create the grid 
    x,y = np.meshgrid(np.array(XBIN2).astype(float),YBIN2)

    #create the figure
    fig = plt.figure(figsize=(18,9))
    ax1 = plt.subplot(121)
    ax2 = plt.subplot(122)
    axes = [ax1,ax2]

    #chosen contourmaps
    cmap1 = plt.get_cmap('bwr', 20)
    cmap2 = plt.get_cmap('rainbow', 13)

    ###################
    #Shade the bias
    im = ax1.pcolormesh(x,y,(BSCHEME),cmap = cmap1,vmin=-20,vmax=20)
    ax1.set_title('Bias (kt) %s, %s'%(MODELNAME,BASIN),fontsize=16)
  
    ####################
    #shade the STDE
    im2 = ax2.pcolormesh(x,y,(USCHEME),cmap = cmap2,vmin=6,vmax=32)
    ax2.set_title('STDE (kt) %s, %s'%(MODELNAME,BASIN),fontsize=16)

    
    #Now loop through points and add the numbers to the plot
    for i in range(len(FBS)):
        for j in range(len(VMS)):
            #Only add the number if it will be within the x and y limits
            if (VMS[j]>=(ylims[0]+5))&(VMS[j]<=(ylims[1]-5))&(int(FBS[i])<(xlims[1])):
                text = ax1.text(FBS[i], VMS[j], (BSCHEME[j, i]),ha="center", va="center", color="k")   
                text = ax2.text(FBS[i], VMS[j], (USCHEME[j, i]),ha="center", va="center", color="k")   




    #loop through the axes and format the subplot
    for ax in axes:
        #Set the x-labels so they are on the midpoint of the x-axis
        ax.set_xticks(FBS)
        ax.set_xticklabels(XBIN)

        #Cut off the edge and increase tick number size
        cutedge(ax)

        #Set yticks and ytick labels
        ax.set_yticks(VMS)
        ylabs = ['%s:%s'%(val[0],val[1]) for val in zip(YBINL,YBINU)]
        ax.set_yticklabels(ylabs, va="center")

        #set the limits
        ax.set_ylim(ylims[0],ylims[1])
        ax.set_xlim(xlims[0],xlims[1])

        ax.set_xlabel('Forecast (hour)',fontsize=14)

    ax1.set_ylabel('Forecasted Intensity Change (kt)',fontsize=14)
    ax2.set_ylabel('Forecasted Intensity Change (kt)',fontsize=14)


    #Adjust the subplots and add the colorbars
    fig.subplots_adjust(hspace=.15,wspace=.15)
    cb = plt.colorbar(im,ax=ax1,ticks=np.linspace(-20,20,20+1),pad=.02,shrink=.8)
    cb.ax.set_title('(kt)',fontsize=14)

    cb = plt.colorbar(im2,ax=ax2,ticks=np.linspace(6,32,14),pad=.02,shrink=.8)
    cb.ax.set_title('(kt)',fontsize=14)


    #if path_to_save is provided, then save the image there
    if path_to_save!=False:
        plt.savefig(path_to_save+'/IBUS_Input_%s_%s.png'%(MODELNAME,BASIN),bbox_inches='tight')
        plt.close(fig)
    else:
        plt.show()

    

if __name__ == "__main__":
    if MAKE_ALL!=True:
        plot_BC_UC(MODELNAME,BASIN,path_to_save)

    elif MAKE_ALL==True:
        BASINS=['AL','EP']
        MODELNAMES=Config_.Model_List

        for BASIN in BASINS:
            for MODELNAME in MODELNAMES:
                plot_BC_UC(MODELNAME,BASIN,'../Output')




