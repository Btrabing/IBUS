###############################################
# Required functions
import numpy as np
from scipy.ndimage import gaussian_filter
import warnings
warnings.filterwarnings("ignore")

from IBUS_util import *


###############################################
'''
Within this code are 4 functions:
    read_vmod(filename): Reads in qkver file and outputs nested lists of the data
    fore_int_diag(forecasts,fint): Sets the parameters and calculates the bias and STDE from read_vmod
    get_IBUS(fpath,find) : Function that calls read_vmod and fore_int_diag and applies smoother
    Output_IBUS(MODELNAME,BASIN,BSCHEME,USCHEME,XBIN,YBINL,YBINU): Takes get_IBUS and outputs to text files

Input to set prior to running:
    filename = 'qkver.dvp.AL_10-19_dshp-lgem': any qkver like file with intensity change at specified F_hours
    MODELNAME = 'DSHP': Model descriptor which is provided to output file only
    MODELIndex = 6 : Index of model in qkver file. If only 1 model in qkver always use 6. If multiple models
                     present in qkver then index needs to be increased based on order of models.
    BASIN ='AL': Basin descriptor which is provided to output file only


Outputs two text files with the bias and uncertainty (standard deviation of errors):
    MODELNAME+'_'+BASIN+'_Bias.txt'
    MODELNAME+'_'+BASIN+'_Uncr.txt'
    

March 1, 2021
Benjamin Trabing, CIRA/CSU/NHC
Ben.Trabing@NOAA.gov
    
'''

###############################################
# Declaritives
#

pathtoqkver = '../Data/QKVER'
output_path = '../Input'


YEAR1 = "2017"
YEAR2 = "2021"

#filename = 'qkver.dvp.AL_10-19_dshp-lgem'
#filenameEP = 'qkver.dvp.EP_10-19_dshp-lgem'
MODELNAME = "OFCL"   #Only used if MAKE_ALL=False
BASIN     = "AL"     #Only used if MAKE_ALL=False
nosmooth  = True #"F121"   #If True, then no smoothing to the data is applied (i.e. for NHC forecasts) else use "Gaussian" or "F121" 


filename = 'qkver_%s-%s_B%s_%s.dat'%(YEAR1,YEAR2,BASIN.lower(),MODELNAME)

#################################
MAKE_ALL=False
#If true, the same smoothing and yyyy bounds will be used for all models
#################################
#
#
#
# Note that for Gaussian smoothing to work properly, you need forecasts at every hour
# For 121 filters there can be missing file times
#
###############################################
# Function Declarations

def read_vmod(filename):
    '''
    For a 3 model qkver file where DSHP, LGEM and NCHG are specified the input will be
    
    Name = 0
    Datetime = 1
    V(0) = 2
    Lat = 3
    Lon = 4
    Best Track Intensity Change = 5
    DSHIPS = 6
    LGEM = 7
    NCHG = 8
    
    Otherwise, index number 6 will always correspond to the desired model intensity change
    
    Code needs to give filename only
    '''
    with open(filename) as f:
        #These are the 12 hourly intensity change values in the qkver files
        Verifying_Hours = np.arange(Config_.F_START,Config_.F_END+Config_.F_INTER,Config_.F_INTER).astype(float)


        #make list of lists to loop through
        forecasts = [list() for num in range(0,len(Verifying_Hours))]
        fc_index = 0

        for line in f: #loop through each line in the file
            Elements = line.split() #split the lines into elements

            if Elements: #if there is data (i.e not empty space)
#                if (Verifying_Hours[fc_index])                
                #First check for if line is a header
                if Elements[0]=='DELV': #If the line has DELV-- this means a header for a forecast hour
                    #The header lines vary in length when split using spaces because t= 12h and t=168h have same total space
    
                    if fc_index == 0: #if this is the first line of the file of the file
                        base_len = len(Elements) #Get the length of the header (this will be the length with the space)

                    #Now chech if header will have the space between the 't=' and the forecast hour
                    if len(Elements)==base_len: #There is a space
                        FF_Name = (Elements[2]) #Get second element which is the forecast hour
                        HEADER = [e for e in Elements if e not in ('t=', 'hr')]

                        # Check that if times extend past what is needed, we break
                        if int(FF_Name)>int(Config_.F_END):
                            break

                        if int(FF_Name)>12: # Skip to next forecast hour list if greater than 12 (the first forecast hour)
                            fc_index += 1 # Next forecast hour index


                    else:
                        #These are the cases where `t=108h` and there is no space
                        FF_Name = (Elements[1][2:]) #Now get the first element and take out the `t='
                        fc_index += 1


                #If the line is not a header, then it is a forecast, add it to the correct forecast list
                elif Elements:
                    forecasts[fc_index].append(Elements[1:])       

    #Return as list of 2D arrays
    return [np.array(ff) for ff in forecasts], HEADER



def fore_int_diag(forecasts,fint,MODEL):
    #
    # Bins the forecasted intensity change and forecast hours, calculates the Bias and STDE
    #
    #
    #These are the forecast hours in the read_vmod
    F_name = np.arange(Config_.F_START,Config_.F_END+Config_.F_INTER,Config_.F_INTER).astype(str)

    #Define the upper and lower bounds in terms of percentiles
    Bound_Tperc = 97 #Top bound
    Bound_Lperc = 100-Bound_Tperc #Lower Bound

    #Intenisty change bins (i.e. bin spacing for the 11th bin) vmin[10]--> vmax[10]
    #This is done to allow for variable bin sizes
    #Note that intensity change is given in integers such that the .5 values are to round to nearest bin
    vmins = np.array([-127.5, -117.5, -107.5, -97.5, -87.5,-77.5, -67.5,-57.5,  -47.5,\
     -37.5, -27.5, -17.5,   -7.5,-2.5,2.5, 7.5,   17.5,   27.5,  37.5, 47.5,   57.5, 67.5, 77.5,\
      87.5,  97.5,  107.5, 117.5])

    vmaxs = np.array([-117.5, -107.5, -97.5, -87.5,-77.5, -67.5,-57.5,  -47.5,\
     -37.5, -27.5, -17.5,   -7.5,-2.5,2.5, 7.5,   17.5,   27.5,  37.5, 47.5,   57.5, 67.5, 77.5,\
      87.5,  97.5,  107.5, 117.5, 127.5])
    
    #Initialize the bias correction fields
    mean_ints = np.zeros(shape=(len(F_name),len(vmins)))
    std_ints = np.zeros(shape=(len(F_name),len(vmins)))

    i=0 #index for forecast length
    for fore in forecasts:
        if len(fore)==0:
            #This means there are no forecasts for this forecast hour length
            #Such as for OFCL forecasts at t=108h
            
            # Loop through intensity change bins and apply np.nan for these cases
            j=0 #index for intensity change
            for vmin,vmax in zip(vmins,vmaxs):
                mean_ints[i,j] = np.nan
                std_ints[i,j] = np.nan
                
                j+=1 #move to next intensity change bin
            
            i+=1 #move to next forecast hour bin
            continue #don't run rest of code block        
            
        if (MODEL=='OFCL')&(F_name[i]==60):
            # There is not a large enough set of 60 h forecasts yet, so remove of OFCL

            # Loop through intensity change bins and apply np.nan for these cases
            j=0 #index for intensity change
            for vmin,vmax in zip(vmins,vmaxs):
                mean_ints[i,j] = np.nan
                std_ints[i,j] = np.nan

                j+=1 #move to next intensity change bin

            i+=1 #move to next forecast hour bin
            continue #don't run rest of code block        



        #Uses the MODEL_index to get the intensity change values for that storm
        forecasted_data = fore[:,fint].astype(np.float)
        observed_data = fore[:,5].astype(np.float)
        
        #Calculate the upper and lower bound intensity change values 
        lower_bound = (np.percentile(forecasted_data,Bound_Lperc,interpolation='lower'))
        upper_bound = (np.percentile(forecasted_data,Bound_Tperc,interpolation='higher'))

        
        #######################
        j=0 #index for intensity change
        for vmin,vmax in zip(vmins,vmaxs):
            if vmax<=lower_bound:
                #If the intensity change is below the 3rd percentile combine all that data and apply
                data =  observed_data[(forecasted_data<=lower_bound)]
                data_f =  forecasted_data[(forecasted_data<=lower_bound)]

            elif vmin>=upper_bound:
                #If the intensity change is above the 97th percentile combine all that data and apply
                data =  observed_data[(forecasted_data>=upper_bound)]
                data_f =  forecasted_data[(forecasted_data>=upper_bound)]

                
            else:
                #parse data into when similar forecasts occurred
                data =  observed_data[(forecasted_data>=vmin)&(forecasted_data<=vmax)]
                data_f =  forecasted_data[(forecasted_data>=vmin)&(forecasted_data<=vmax)]


            #If there is data in the bin, there should be
            #Calculate the bias (mean of distribution) and standard deviation of the errors
            if data.any():
                mean_ints[i,j] = np.nanmean(data_f-data)
                std_ints[i,j] = np.nanstd(data_f-data)

 
            #If there is no forecast in the bin
            #Should not happen, but just in case
            else:
                mean_ints[i,j] = np.nan
                std_ints[i,j] = np.nan
                
            j+=1 #move to next intensity change bin
            
        i+=1 #move to next forecast hour bin
                

    #Output the Bias, STDE, forecast hours, vmin bins, and vmax bins        
    return mean_ints,std_ints,F_name,vmins,vmaxs


def filter_121(data,N=2):
    '''
    Requirements: 
    import numpy as np
    This function will smooth a 2D array using a 121 filter with weights of .25,.5,.25. Include an N value
    which will iteterate the function the specified number of times.
    The 121 filter reverses direction on each odd itter, so N=3 will have 2 iterations from the bottom and 1 itteration from the top
    At the boundaries  there are weights of .5,.5 to allow some smoothing of endpoints.
    Outputs an array with the same length as the input array
    '''
    
    fil_data = np.copy(data)
     
    
    for rn,arr in enumerate(data):

        fil_arr = np.empty(len(arr))
        
            
        count = 0
        while count<N:
            if count%2==1: #We have an odd (flip arr) 
                arr = arr[::-1]
                
            for i in np.arange(0,len(arr)):
                if i ==0:
                    fil_arr[i] = .5*arr[i]+ .5*arr[i+1]
                elif i==(len(arr)-1):
                    fil_arr[i] = .5*arr[i-1]+ .5*arr[i]
                else:
                    fil_arr[i] = .25*arr[i-1]+.5*arr[i]+.25*arr[i+1]
                    
            count = count+1
            
            if count%2==1: #We have an odd so (flip arr) 
                arr = fil_arr[::-1]
            else: 
                arr = fil_arr
        
        fil_data[rn]=arr
        
    return fil_data


def get_IBUS(fpath,MODEL,nosmooth=False):
    #
    # Calls read_vmod and fore_int_diag then returns IBUS with bins and f_hours
    #
    #
    #Read in the data from qkver file
    Database,HEADER = read_vmod(fpath)
    f_ind = HEADER.index(MODEL)

    
    #Use the data to calculate the biase and STDE and output the bins specified in code
    mean_,std_,xbin,ybinl,ybinu = fore_int_diag(Database,f_ind,MODEL) 
    
    if nosmooth == True:
        return mean_,std_,xbin,ybinl,ybinu

    elif nosmooth=='Gaussian':
        return gaussian_filter(mean_,2),gaussian_filter(std_,2),xbin,ybinl,ybinu

    elif nosmooth=='F121':
        return filter_121(mean_),filter_121(std_),xbin,ybinl,ybinu
        



def Output_IBUS(MODELNAME,BASIN,BSCHEME,USCHEME,XBIN,YBINL,YBINU,Output_Dir):
    #
    # Takes the IBUS and produces 2 output files for the bias and uncertainty
    #
    #
    #Define the output file names for the bias and uncertainty scheme
    Ouput_name_B = Output_Dir+'/'+MODELNAME+'_'+BASIN+'_Bias.txt'
    Ouput_name_U = Output_Dir+'/'+MODELNAME+'_'+BASIN+'_Uncr.txt'
    
    #Open the bias file to write
    with open(Ouput_name_B, 'w') as fp:
        
        Header_ = [MODELNAME] + [BASIN] + list(XBIN) # list of header info
        Header = '\t'.join([format(el, '>6') for el in Header_]) #tab delimit header into right aligned columns
        fp.write(Header) #write to file
        fp.write('\n')   #Move to next line
        for i in np.arange(0,BSCHEME.shape[1]): #Loop through intensity change thresholds since more of them
            Row_ = [YBINL[i]] + [YBINU[i]] + list(BSCHEME[:,i])#add the lower and upper intensity change limits to row
            Row = '\t'.join([format("%03.1f"%el, '>6') for el in Row_]) #tab delimit header into right aligned columns
            fp.write(Row)  #write to file
            fp.write('\n') #Move to next line
    
    #Open the STDE file to write
    with open(Ouput_name_U, 'w') as fp:

        Header_ = [MODELNAME] + [BASIN] + list(XBIN) # list of header info
        Header = '\t'.join([format(el, '>6') for el in Header_]) #tab delimit header into right aligned columns
        fp.write(Header) #write to file
        fp.write('\n')   #Move to next line  
        for i in np.arange(0,USCHEME.shape[1]): #Loop through intensity change thresholds since more of them
            Row_ = [YBINL[i]] + [YBINU[i]] + list(USCHEME[:,i])#add the lower and upper intensity change limits to row
            Row = '\t'.join([format("%03.1f"%el, '>6') for el in Row_]) #tab delimit header into right aligned columns
            fp.write(Row)  #write to file
            fp.write('\n') #Move to next line

###############################################
# Run the functions

if __name__ == "__main__":
    if MAKE_ALL==False:

        #Read data, develop the bias correction and STDE output, and return the smoothed IBUS
        BSCHEME,USCHEME,XBIN,YBINL,YBINU = get_IBUS(pathtoqkver+'/'+filename,MODELNAME,nosmooth)
    
        #Out the IBUS into two text files to be used operationally
        Output_IBUS(MODELNAME,BASIN,BSCHEME,USCHEME,XBIN,YBINL,YBINU,output_path)
    
        print('Bias Correction and Uncertainty Estimator Files Produced')
    
    elif MAKE_ALL==True:
        BASINS=['AL','EP']
        MODELNAMES=Config_.Model_List

        for BASIN in BASINS:
            for MODELNAME in MODELNAMES:
                filename = 'qkver_'+YEAR1+'-'+YEAR2+'_B'+BASIN.upper()+'_'+MODELNAME+'.dat'

                #Read data, develop the bias correction and STDE output, and return the smoothed IBUS
                BSCHEME,USCHEME,XBIN,YBINL,YBINU = get_IBUS(pathtoqkver+'/'+filename,MODELNAME,nosmooth)

                #Out the IBUS into two text files to be used operationally
                Output_IBUS(MODELNAME,BASIN,BSCHEME,USCHEME,XBIN,YBINL,YBINU,output_path)

                print('IBUS Files Produced for %s in %s'%(MODELNAME,BASIN))


