#!/usr/bin/env python
###############################################
import datetime as dt
import glob
import numpy as np
import warnings
warnings.filterwarnings("ignore")

from IBUS_util import *
from read_IBUS_input import *
from IBUS_main import *
from Plot_IBUS import *

###############################################
# This is a python wrapper script that runs IBUS to
# create the text file and then creates the plots
# 
# Ben Trabing
# Ben.Trabing@noaa.gov
#
###############################################
# Declaritives
#

pathtoadeck  = '../Data/AID/'
pathtoscheme = '../Input/'
MODELS       = Config_.Model_List
BASINS         = ['EP','AL']

#If operationall run, set OPER to true
#If testing, set to False and specify datetime to run
OPER=False
OFF_DATES = ['2021082200']

# To use a list of 6 hourly forecasts to run the IBUS on, uncomment the line below and set the start and end date using YYYY MM DD HH 
#OFF_DATES = get_alltime(dt.datetime(2021,5,1,0),dt.datetime(2021,11,1,12))

Output_Loc_TXT = '../Output/TXTs/'
Output_Loc_FIG = '../Output/Figs/'

###############################################
# Function Declarations

 
if __name__ == "__main__":

    for BASIN in BASINS:

        if OPER==True:
            run_IBUS(MODELS,BASIN,pathtoadeck,pathtoscheme,Output_Loc_TXT,OPER)
            plot_all_IBUS(BASIN,Output_Loc_TXT,Output_Loc_FIG,OPER)

            try:
                for MODEL in MODELS:
                    plot_MODEL_IBUS_P(BASIN,Output_Loc_TXT,MODEL,Output_Loc_FIG,OPER)
            except:
                pass

        
        elif OPER==False:
            for OFF_Date in OFF_DATES:
                try:
                    run_IBUS(MODELS,BASIN,pathtoadeck,pathtoscheme,Output_Loc_TXT,OFF_Date)
                    plot_all_IBUS(BASIN,Output_Loc_TXT,Output_Loc_FIG,OFF_Date)

                    for MODEL in MODELS:
                        plot_MODEL_IBUS_P(BASIN,Output_Loc_TXT,MODEL,Output_Loc_FIG,OFF_Date)

                except:
                    pass
     
    
